# Robust Monkey Experiments

This project contais an engine that can interact with a cloud application deployed with trans-cloud orchestrator. 
Mokey engine injects different kind of errors in both application and infrastructure side.

### Build and Run 

You can build the project running the command

````
mvn clean install
````

You can generate a plan of failures to be injected by monkey by means of the class
````
org.scenic.monkey.generator.MonkeyBluePrintGenerator
````

This plan can be used over a running application using the class 

````
org.scenic.monkey.generator.MonkeyBluePrintExecutor
````

## Use Case. 24h
To test this engine the Softcare application has been deployed using a trans-cloud orchestrator ([see here the topology]((https://gitlab.com/kiuby88/robust-monkey/-/blob/master/src/main/resources/monkey/app/app-brooklyn.yaml)), and a plan of failures has been generated to be injected during 24 hour ([see here the failires](https://gitlab.com/kiuby88/robust-monkey/-/blob/master/monkey/monkey-play-24-hours-8-5/monkey-blueprint.json))

Below you can see a image that show how failures have been injected in the application over the time.

![monkey24h](https://gitlab.com/kiuby88/robust-monkey/-/raw/ea218c34d82cddd33761bf7c1715bb12dd4b3a03/monkey-24h.png)

The black linke in the image means how many application components are running correctly over the time. Yellow vertical lines represent when an error has been inyected. Three different representative periods have been zoomed to illustrate how errors are injected. You can see after the injections the number of running applications goes down, and how the trans-cloud robust system reacts to failires and restores the application.

In addition of the injections, th monkey engines is able to extract the application status and log them in order. You can see [here](https://gitlab.com/kiuby88/robust-monkey/-/tree/master/monkey/monkey-play-24-hours-8-5/healthcheck) the complete logs with the information that has been used to compose the image above.
