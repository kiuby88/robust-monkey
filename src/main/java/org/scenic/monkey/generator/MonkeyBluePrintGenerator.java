package org.scenic.monkey.generator;

import static org.scenic.monkey.dto.MonkeyOperation.CRASH;
import static org.scenic.monkey.dto.MonkeyOperation.STOP;
import static org.scenic.monkey.dto.Provider.AWS;
import static org.scenic.monkey.dto.Provider.PIVOTAL;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.scenic.monkey.dto.MonkeyBluePrint;
import org.scenic.monkey.dto.MonkeyTask;

/**
 * Created by Jose on 10/12/19.
 */
public class MonkeyBluePrintGenerator {

    private static int HOURS = 24;
    private static int TOTAL = 3600 * HOURS;

    private static double AV_MINUTES = 8;
    private static double STD_DSV_MIN = 5;

    Random random = new Random();
    List<MonkeyTask> tasks = MonkeyBluePrintGenerator.tasks();

    String suffix = HOURS + "-hours-";

    public static void main(String[] args) throws FileNotFoundException {

        MonkeyBluePrintGenerator m = new MonkeyBluePrintGenerator();
        List<MonkeyTask> generatedTasks = m.generateMonkeyBluePrint(TOTAL);
        m.printMonkeyBluePrint(generatedTasks);

        for (MonkeyTask t : generatedTasks) {
            System.out.println(t.toString());
        }
    }


    public List<MonkeyTask> generateMonkeyBluePrint(int limit) {

        List<MonkeyTask> generatedTasks = new LinkedList<>();
        int total = 0;
        int index = 0;
        while (total <= limit) {
            int time = getNextTime();
            total = total + time;
            generatedTasks.add(getNextTask().withTimeBefore(time).withIndex(index));
            index++;
        }
        return generatedTasks;
    }

    private MonkeyTask getNextTask() {
        MonkeyTask result = tasks.get(random.nextInt(9));
        return new MonkeyTask(result.getEntity(), result.getOperation(), result.getProvider());
    }

    public Integer getNextTime() {
        return new Double(random.nextGaussian() * (STD_DSV_MIN + 60) + (AV_MINUTES * 60)).intValue();
    }

    private static List<MonkeyTask> tasks() {
        List<MonkeyTask> tasks = new LinkedList<>();

        tasks.add(new MonkeyTask("Softcare_dashboard", CRASH, AWS));
        tasks.add(new MonkeyTask("Softcare_dashboard", STOP, AWS));

        tasks.add(new MonkeyTask("Forum", CRASH, PIVOTAL));
        tasks.add(new MonkeyTask("Forum", STOP, PIVOTAL));
        tasks.add(new MonkeyTask("ForumDB", STOP, AWS));
        tasks.add(new MonkeyTask("ForumDB", CRASH, AWS));

        tasks.add(new MonkeyTask("SoftcareWS", CRASH, AWS));
        tasks.add(new MonkeyTask("SoftcareWS", STOP, AWS));
        tasks.add(new MonkeyTask("SoftcareDB", STOP, AWS));
        tasks.add(new MonkeyTask("SoftcareDB", CRASH, AWS));

        //tasks.add(new MonkeyTask("Multimedia", CRASH, AWS));
        //tasks.add(new MonkeyTask("Multimedia", STOP, AWS));
        //tasks.add(new MonkeyTask("MultimediaDB", CRASH, AWS));

        return tasks;
    }


    public void printMonkeyBluePrint(List<MonkeyTask> data) throws FileNotFoundException {
        String folder = String.format("monkey/%s[%s,%s]monkey-play-%s-%s", HOURS,AV_MINUTES, STD_DSV_MIN, suffix, Instant.now().toString());
        new File(folder).mkdir();

        PrintWriter writer = new PrintWriter(folder + File.separator + "monkey-blueprint.json");
        writer.println(new MonkeyBluePrint(data).toJson());
        writer.close();
    }


}



