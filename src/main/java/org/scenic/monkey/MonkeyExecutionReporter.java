package org.scenic.monkey;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.scenic.monkey.dto.BrooklynApplicationStatus;
import org.scenic.monkey.dto.MonkeyTask;
import org.scenic.monkey.loader.MonkeyBlueprintLoader;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

/**
 * Created by Jose on 11/12/19.
 */
public class MonkeyExecutionReporter {


    public static void main(String[] args) throws Exception {

        String scenario = "24[8.0,5.0]monkey-play-24-hours--2020-02-04T21:11:56.256Z";

        List<BrooklynApplicationStatus> status = loadAllElementsFromFolder(scenario);

        List<EntitisRunningAndTime> l = mapEntitisRunningAndTime(status);

        printCsv(scenario, l.stream().map(EntitisRunningAndTime::toCsvRow).collect(toList()), "entitiesHistory", "time;numEntities");

        List<MonkeyTask> monkeyTasks = MonkeyBlueprintLoader.loadTasks(scenario);
        List<String> erroTimes = accumulateTimeForInjections(monkeyTasks);
        printCsv(scenario, erroTimes, "injections", "injection");

        copyFiles(scenario);
    }


    private static void copyFiles(String folder) throws IOException {
        CopyOption[] options = new CopyOption[]{StandardCopyOption.REPLACE_EXISTING};

        String targetFolder = folder.replace(":", "-").replace(".", "-");
        getTargetLocation(targetFolder, "").toFile().mkdirs();

        Files.copy(
                getLocation(folder, "entitiesHistory.csv"),
                getTargetLocation(targetFolder, "entitiesHistory.csv"), StandardCopyOption.REPLACE_EXISTING);
        Files.copy(getLocation(folder, "injections.csv"),
                getTargetLocation(targetFolder, "injections.csv"), StandardCopyOption.REPLACE_EXISTING);


        Files.copy(
                getLocation(folder, "before.csv"),
                getTargetLocation(targetFolder, "before.csv"), StandardCopyOption.REPLACE_EXISTING);
        Files.copy(getLocation(folder, "after.csv"),
                getTargetLocation(targetFolder, "after.csv"), StandardCopyOption.REPLACE_EXISTING);


    }

    private static Path getLocation(String folder, String fileName) {
        return Paths.get(String.format("monkey/%s/reports/%s", folder, fileName));
    }

    private static Path getTargetLocation(String folder, String fileName) {
        return Paths.get(String.format("/Users/Jose/papers/robust-parallel/evaluation/monkey/%s/%s", folder, fileName));
    }


    private static List<String> accumulateTimeForInjections(List<MonkeyTask> monkeyTasks) {
        System.out.println("---------------\n\n\n\n\n\n\n");
        List<String> l = new LinkedList<>();
        long time = 0;
        for (MonkeyTask t : monkeyTasks) {
            time = time + t.getTimeBefore();
            System.out.println(time + " " + t.getEntity() + " " + t.getOperation());

            l.add(String.valueOf(time));
        }
        return l;
    }

    private static void printCsv(String scenario, List<String> l, String name, String header) throws Exception {

        String reportsFolder = "monkey/" + scenario + "/reports/";

        new File(reportsFolder).mkdir();

        PrintWriter writer = new PrintWriter(reportsFolder + File.separator + name + ".csv");
        writer.println(header);
        for (String r : l) {
            writer.println(r);
        }
        writer.close();
    }

    private static List<EntitisRunningAndTime> mapEntitisRunningAndTime(List<BrooklynApplicationStatus> allStatus) {

        List<EntitisRunningAndTime> l = new LinkedList<>();
        for (BrooklynApplicationStatus status : allStatus) {
            long observed = status.getObservedAt().toEpochMilli();
            long base = status.getBaseTime().toEpochMilli();
            long time = ((observed - base) / 1000);
            l.add(new EntitisRunningAndTime(String.valueOf(time), String.valueOf(runningEntities(status))));
        }
        return l;
    }

    private static Long runningEntities(BrooklynApplicationStatus status) {

        return status.getAppStatus().entrySet().stream()
                .filter(e -> !("SoftcareApp".equalsIgnoreCase(e.getKey())))
                .map(Map.Entry::getValue)
                .filter(m -> "running".equalsIgnoreCase(m) || "run".equalsIgnoreCase(m))
                .count();

        /*return status.getAppStatus().values()
                .stream()
                .map(String::toLowerCase)
                .filter(m -> "running".equals(m) || "run".equals(m))
                .count();*/
    }

    private static List<BrooklynApplicationStatus> loadAllElementsFromFolder(String scenario) throws Exception {

        String healthCheckFolder = "monkey/" + scenario + "/healthcheck";

        Stream<Path> paths = Files.walk(Paths.get(healthCheckFolder));
        paths
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .forEach(System.out::println);

        return Files.walk(Paths.get(healthCheckFolder))
                .filter(Files::isRegularFile)
                .filter(p -> !p.getFileName().toString().contains("DS_Store"))
                .map(MonkeyExecutionReporter::mapToStatus)
                .collect(toList());
    }

    private static BrooklynApplicationStatus mapToStatus(Path p) {

        Gson gs = new Gson();
        try {
            BrooklynApplicationStatus s = gs.fromJson(new JsonReader(new FileReader(p.toFile().toString())), BrooklynApplicationStatus.class);
            int i = 0;
            System.out.println("--------------------------------- " + p.getFileName());
            System.out.println("seconds =>" + (s.getObservedAt().getEpochSecond() - s.getBaseTime().getEpochSecond()));
            System.out.printf("running=> " + s.getAppStatus().entrySet().stream()
                    .filter(e -> !e.getKey().equalsIgnoreCase("softcareapp"))
                    .filter(e -> e.getValue().equalsIgnoreCase("running"))
                    .count() + "\n");

            //s.setBaseTime(null);
            //s.setObservedAt(null);

            s.getAppStatus().remove("SoftcareApp");

            System.out.println(s.toStringAsJson());
            return s;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /*private ExperimentExecutionTrace mapToMigrationTasks(Path p) {
        Gson gs = new Gson();
        try {
            return gs.fromJson(new JsonReader(new FileReader(p.toFile().toString())), ExperimentExecutionTrace.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }*/


    public static class EntitisRunningAndTime {
        private String time;
        private String runningEntities;


        public EntitisRunningAndTime(String time, String runningEntities) {

            this.time = time;
            this.runningEntities = runningEntities;
        }


        public String getRunningEntities() {
            return runningEntities;
        }

        public void setRunningEntities(String runningEntities) {
            this.runningEntities = runningEntities;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String toCsvRow() {
            return String.format("%s;%s", this.time, this.runningEntities);
        }
    }


}
