package org.scenic.monkey.loader;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.scenic.monkey.dto.MonkeyBluePrint;
import org.scenic.monkey.dto.MonkeyTask;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

/**
 * Created by Jose on 11/12/19.
 */
public class MonkeyBlueprintLoader {


    public static List<MonkeyTask> loadTasks(String folder) throws IOException {
        return mapToMigrationTasks(Paths.get("monkey/" + folder + "/monkey-blueprint.json"));
    }

    private static List<MonkeyTask> mapToMigrationTasks(Path p) {
        Gson gs = new Gson();
        try {
            MonkeyBluePrint f = (MonkeyBluePrint) gs.fromJson(new JsonReader(new FileReader(p.toFile().toString())), MonkeyBluePrint.class);
            return f.getTasks();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
