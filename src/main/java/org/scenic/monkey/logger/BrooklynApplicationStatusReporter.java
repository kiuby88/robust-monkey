package org.scenic.monkey.logger;

import static java.time.Instant.now;

import java.io.File;
import java.io.PrintWriter;
import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.scenic.monkey.deployer.BrooklynApplicationStatusExtractor;
import org.scenic.monkey.dto.BrooklynApplicationStatus;

/**
 * Created by Jose on 11/12/19.
 */
public class BrooklynApplicationStatusReporter {
    ExecutorService executorService = Executors.newFixedThreadPool(2);
    private String folder;
    public BrooklynApplicationStatusExtractor extractor;
    private BrooklynApplicationStatus previousStatus = null;

    public BrooklynApplicationStatusReporter(BrooklynApplicationStatusExtractor extractor, String path) {
        this.extractor = extractor;
        folder = "monkey/" + path + "/healthcheck";
        checkIfHealthServiceExist();
    }

    public void close(){
        executorService.shutdownNow();
    }

    private void checkIfHealthServiceExist() {
        if (new File(folder).exists()) {
            throw new RuntimeException("Folder of healthcheck should not exist: " + folder);
        }
    }

    public void report(String applicationId, Instant timeBase) {
        BrooklynApplicationStatus currentStatus = extractor.getStatus(applicationId, timeBase);
        if (isDifferentThanPrev(currentStatus)) {
            previousStatus = currentStatus;
            addToReport(currentStatus);
        }
    }

    private boolean isDifferentThanPrev(BrooklynApplicationStatus current) {
        if (previousStatus == null) {
            return true;
        }
        return !previousStatus.getAppStatus().equals(current.getAppStatus());
    }

    private void addToReport(BrooklynApplicationStatus status) {
        try {
            new File(folder).mkdir();

            PrintWriter writer = new PrintWriter(folder + File.separator + "status-" + now() + ".json");
            writer.println(status.toStringAsJson());
            writer.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Future<String> executeReporter(String applicationId, Instant timeBase) {
        Future<String> future = executorService.submit(() -> {
            while (true) {
                try {
                    report(applicationId, timeBase);
                    Thread.sleep(7_000);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
        return future;
    }

    public static class ThreadR extends Thread {

        private BrooklynApplicationStatusReporter r;
        private String appId;
        private Instant timeBase;
        private boolean go = true;

        public ThreadR(BrooklynApplicationStatusReporter r, String appId, Instant timeBase) {
            this.r = r;
            this.appId = appId;
            this.timeBase = timeBase;
        }

        @Override
        public void run() {
            while (go) {
                try {
                    Thread.sleep(5);
                    r.report(appId, timeBase);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }


}
