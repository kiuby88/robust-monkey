package org.scenic.monkey;

import static java.time.Instant.now;
import static java.util.Collections.singletonList;
import static org.scenic.monkey.dto.MonkeyOperation.CRASH;
import static org.scenic.monkey.dto.MonkeyOperation.STOP;
import static org.scenic.monkey.dto.Provider.AWS;
import static org.scenic.monkey.dto.Provider.PIVOTAL;

import java.io.File;
import java.io.PrintWriter;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.scenic.cleaner.entity.AwsEntityCleaner;
import org.scenic.cleaner.entity.CloudFoundryEntityCleaner;
import org.scenic.cleaner.entity.EntitiesStopper;
import org.scenic.monkey.deployer.BrooklynApplicationDeployer;
import org.scenic.monkey.deployer.BrooklynApplicationStatusExtractor;
import org.scenic.monkey.dto.MonkeyTask;
import org.scenic.monkey.loader.MonkeyBlueprintLoader;
import org.scenic.monkey.logger.BrooklynApplicationStatusReporter;
import org.scenic.parallel.clients.OrchestratorClient;
import org.scenic.proxy.DeployerProxy;

/**
 * Created by Jose on 10/12/19.
 */
public class MonkeyBluePrintExecutor {

    DeployerProxy deployer = DeployerProxy.connectDefault();
    BrooklynApplicationDeployer brooklynApplicationDeployer = new BrooklynApplicationDeployer(deployer);
    OrchestratorClient orchestratorClient = OrchestratorClient.build();
    EntitiesStopper entitiesStopper = new EntitiesStopper(deployer);
    AwsEntityCleaner awsEntityCleaner = new AwsEntityCleaner(deployer);
    CloudFoundryEntityCleaner cloudFoundryEntityCleaner = new CloudFoundryEntityCleaner(deployer);
    InjectionLogger injectionLogger;

    public String applicationId;


    public static void main(String[] args) throws Exception {
        System.out.println("[Starting with]");
        String scenario = "24[8.0,5.0]monkey-play-24-hours--2020-02-04T21:11:56.256Z";
        BrooklynApplicationStatusReporter reporter = new BrooklynApplicationStatusReporter(new BrooklynApplicationStatusExtractor(new MonkeyBluePrintExecutor().deployer), scenario);
        Future<String> task = null;
        MonkeyBluePrintExecutor executor = new MonkeyBluePrintExecutor();
        executor.injectionLogger = new InjectionLogger(scenario);
        String appId = null;
        try {


            List<MonkeyTask> monkeyTasks = MonkeyBlueprintLoader.loadTasks(scenario);
            show(monkeyTasks);

            appId = executor.deployAndWait();

            executor.applicationId = appId;
            executor.syncWithOrchestrator(appId);

            for (MonkeyTask t : monkeyTasks) {
                if (task == null) {
                    Instant baseTime = now();
                    task = reporter.executeReporter(appId, baseTime);
                    executor.injectionLogger.setBaseTime(baseTime);

                }
                executor.executeTask(t);
            }

            Thread.sleep(120_000);
            executor.waitApplicationWillBeUp(appId);

        } finally {
            task.cancel(true);
            reporter.close();
        }
    }

    private void print(String message) {
        System.out.println(String.format("[%s:] %s", now(), message));
    }

    public boolean waitApplicationWillBeUp(String appId) {
        return brooklynApplicationDeployer.waitApplicationWillBeUp(appId);
    }

    public void executeTask(MonkeyTask t) throws Exception {

        print("Sleeping for " + t.toString());
        print("Wake up about " + now().plusMillis(t.getTimeBefore() * 1000l));
        Thread.sleep(t.getTimeBefore() * 1000l);
        print(">>> Executing task: " + t.toString());
        System.out.println();
        injectionLogger.logBefore();
        if (t.getOperation() == STOP) {
            stop(t);
        } else if (t.getOperation() == CRASH) {
            tryCrash(t);
        } else {
            throw new RuntimeException("Operation not supported: " + t.getOperation());
        }
        injectionLogger.logAfter();
    }

    private void tryCrash(MonkeyTask t) throws InterruptedException {


        boolean crashed = false;
        int limit = 0;
        while (!crashed && limit < 20) {
            try {
                crash(t);
                crashed = true;
            } catch (Exception e) {
                System.out.println("Error  crashing. RETRY because: " + e.getMessage());
                System.out.println("and " + e.getCause());
                System.out.println();
                Thread.sleep(1000);
                limit++;
            }
        }
        if (!crashed) {
            System.out.println("========>ERROR: executing and reexecuting task but entities could not be crashed: " + t);
        }
    }


    private void crash(MonkeyTask t) {
        try {
            if (t.getProvider() == AWS) {
                awsEntityCleaner.cleanEntities(applicationId, splitNames(t.getEntity()));
            } else if (t.getProvider() == PIVOTAL) {
                cloudFoundryEntityCleaner.cleanEntities(applicationId, singletonList(t.getEntity()));
            }
        } catch (Exception e) {
            System.out.println("[CRASH]: error in " + t.toString());
            throw new RuntimeException(e);
        }
    }

    //this is a niapa for boxplot
    private List<String> splitNames(String name){
        if(name.contains("###")){
            return Arrays.stream(name.split("###")).collect(Collectors.toList());
        }
        return singletonList(name);

    }

    private void stop(MonkeyTask t) {
        System.out.println("[STOP:] " + t.getEntity());
        entitiesStopper.stopEntity(applicationId, t.getEntity());
    }

    private String deployAndWait() throws Exception {
        applicationId = brooklynApplicationDeployer.deployAndWait("monkey/app/app-brooklyn.yaml");
        return applicationId;
    }

    private void syncWithOrchestrator(String appId) throws Exception {
        System.out.println("Sync application...");
        orchestratorClient.sycnFromPath(appId, "monkey/app/app-mm.yaml");
    }


    public static void show(List<MonkeyTask> l) {
        System.out.println("BLUEPRINT");
        System.out.println("--------------------");
        for (MonkeyTask m : l) {
            System.out.println(m.toString());
        }
        System.out.println("--------------------");
        System.out.println();
        System.out.println();
        System.out.println();
    }

    public static class InjectionLogger {
        private PrintWriter before;// = new PrintWriter(reportsFolder + File.separator + name + ".csv");
        private PrintWriter after;
        private Instant baseTime;
        private String baseDirectory;

        public InjectionLogger(String baseDirectory) {
            this.baseDirectory = baseDirectory;
            createFiles();
        }

        public void setBaseTime(Instant baseTime) {
            this.baseTime = baseTime;
        }

        private void createFiles() {
            System.out.println("Creating directories");
            String reportsFolder = "monkey/" + baseDirectory + "/reports/";

            new File(reportsFolder).mkdir();
            try {
                before = new PrintWriter(reportsFolder + File.separator + "before.csv");
                before.println("before");
                before.flush();
                System.out.println("created: before");
            } catch (Exception e) {
                System.out.println("Error creating file before " + e.getMessage() + e.getStackTrace());
            }
            try {
                after = new PrintWriter(reportsFolder + File.separator + "after.csv");
                after.println("after");
                after.flush();
                System.out.println("created: after");
            } catch (Exception e) {
                System.out.println("Error creating file after" + e.getMessage() + e.getStackTrace());
            }

        }


        public void logBefore() {
            before.println(now().getEpochSecond() - baseTime.getEpochSecond());
            before.flush();
        }

        public void logAfter() {
            after.println(now().getEpochSecond() - baseTime.getEpochSecond());
            after.flush();
        }


        public void close() {
            tryToClose(before, "before");
            tryToClose(after, "after");
        }

        private void tryToClose(PrintWriter w, String name) {
            try {
                w.close();
            } catch (Exception e) {
                System.out.println("Error closing " + name);
            }
        }


    }
}
