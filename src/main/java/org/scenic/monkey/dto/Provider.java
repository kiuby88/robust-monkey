package org.scenic.monkey.dto;

/**
 * Created by Jose on 10/12/19.
 */
public enum Provider {
    AWS, PIVOTAL
}
