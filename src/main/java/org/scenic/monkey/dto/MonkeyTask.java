package org.scenic.monkey.dto;

/**
 * Created by Jose on 10/12/19.
 */
public class MonkeyTask {

    private String entity;
    private MonkeyOperation operation;
    private Long timeBefore;
    private Provider provider;
    private int index;

    public MonkeyTask(String entity,
                      MonkeyOperation operation,
                      Provider provider) {
        this.entity = entity;
        this.operation = operation;
        this.provider = provider;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public MonkeyOperation getOperation() {
        return operation;
    }

    public void setOperation(MonkeyOperation operation) {
        this.operation = operation;
    }

    public Long getTimeBefore() {
        return timeBefore;
    }

    public MonkeyTask withTimeBefore(long timeBefore) {
        this.timeBefore = timeBefore;
        return this;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String toString() {
        return String.format("[%s] %s %s %s %s", this.index, this.getEntity(), this.getOperation(), this.getProvider(), this.getTimeBefore());
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public MonkeyTask withIndex(int index) {
        this.index = index;
        return this;
    }
}
