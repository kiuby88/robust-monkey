package org.scenic.monkey.dto;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Jose on 11/12/19.
 */
public class BrooklynApplicationStatus {

    private Instant baseTime;
    private Instant observedAt;
    private Map<String, String> appStatus;
    private String relativeCreateAtSeconds;

    public BrooklynApplicationStatus() {
        appStatus = new HashMap<>();
    }

    public Instant getBaseTime() {
        return baseTime;
    }

    public Instant getObservedAt() {
        return observedAt;
    }


    public Map<String, String> getAppStatus() {
        return appStatus;
    }

    public void setObservedAt(Instant observedAt) {
        this.observedAt = observedAt;
    }

    public void setAppStatus(Map<String, String> appStatus) {
        this.appStatus = appStatus;
    }

    public void setBaseTime(Instant baseTime) {
        this.baseTime = baseTime;
    }

    public BrooklynApplicationStatus addEntityStatus(String entity, String status) {
        appStatus.put(entity, status);
        return this;
    }

    public BrooklynApplicationStatus withObservedAt(Instant observedAt) {
        this.observedAt = observedAt;
        return this;
    }

    public BrooklynApplicationStatus withAppStatus(Map<String, String> appStatus) {
        this.appStatus = appStatus;
        return this;
    }

    public BrooklynApplicationStatus withBaseTime(Instant baseTime) {
        this.baseTime = baseTime;
        return this;
    }

    public String toStringAsJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

    public void setRelativeCreateAtSeconds(String s){
        this.relativeCreateAtSeconds=s;
    }

    public BrooklynApplicationStatus withRelativeCreateAtSeconds(String s){
        this.relativeCreateAtSeconds=s;
        return this;
    }

    public String getRelativeCreateAtSeconds(){
        return this.relativeCreateAtSeconds;
    }


}