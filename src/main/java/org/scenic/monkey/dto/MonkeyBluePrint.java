package org.scenic.monkey.dto;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Jose on 10/12/19.
 */
public class MonkeyBluePrint {

    private List<MonkeyTask> tasks;

    public MonkeyBluePrint(List<MonkeyTask> tasks){
        this.tasks=tasks;
    }

    public List<MonkeyTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<MonkeyTask> tasks) {
        this.tasks = tasks;
    }

    public String toJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
