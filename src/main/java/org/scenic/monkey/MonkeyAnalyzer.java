package org.scenic.monkey;

import static java.util.stream.Collectors.toList;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.scenic.monkey.dto.BrooklynApplicationStatus;
import org.scenic.monkey.dto.MonkeyBluePrint;
import org.scenic.monkey.dto.MonkeyTask;
import org.scenic.monkey.loader.MonkeyBlueprintLoader;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

/**
 * Created by Jose on 16/02/20.
 */
public class MonkeyAnalyzer {

    public static void main(String[] args) throws Exception {

        String scenario = "24[8.0,5.0]monkey-play-24-hours--2020-02-04T21:11:56.256Z";
        loadAllElementsFromFolder(scenario);
        loadBeforeAndErrors(scenario);
        calculatePeriods(scenario);

    }

    private static void calculatePeriods(String scenario) throws Exception {

        System.out.println("Period error for");

        LinkedList<Integer> injections = injectionsDuringRecovery();
        ArrayList<BrooklynApplicationStatus> status = (ArrayList<BrooklynApplicationStatus>) loadAllElementsFromFolder(scenario);
        LinkedList<Period> periods = new LinkedList<>();
        for (Integer injection : injections) {
            periods.add(getPeriod(status, injection));
        }
        List<Period> periods2 = new HashSet<>(periods)
                .stream()
                .collect(toList())
                .stream()
                .sorted((p1, p2) -> p1.from.compareTo(p2.from))
                .collect(toList());

        System.out.println("Periods");
        System.out.println();
        System.out.println("From;To;StatusFrom;StatusTo");
        for (Period p : periods2) {
            System.out.println(p.stringAsCsv());
        }


    }

    private static Period getPeriod(ArrayList<BrooklynApplicationStatus> status, Integer injection) {

        int from = injection;

        while ((from >= 0) && !checkAllRunning(status.get(from).getAppStatus())) {
            from--;
        }
        if (from > 0) {
            from--;
        }
        int to = injection;

        while (!checkAllRunning(status.get(to).getAppStatus())) {
            to++;
        }
        to++;

        //return Pair.of(getTime(status.get(from)), getTime(status.get(to)));
        int toForTime=to-1;
        int fromForTime=from+1;
        return new Period(getTime(status.get(fromForTime)), getTime(status.get(toForTime)), from, to);
    }

    private static boolean checkAllRunning(Map<String, String> status) {
        if (status.size() != 5) {
            return false;
        }

        return status.values().stream().allMatch(v -> v.equalsIgnoreCase("running"));

    }

    public static class Period {
        private final Long from;
        private final Long to;
        private final Integer statusFrom;
        private final Integer statusTo;

        public Period(Long from, Long to, Integer statusFrom, Integer statusTo) {
            this.from = from;
            this.to = to;
            this.statusFrom = statusFrom;
            this.statusTo = statusTo;
        }

        public String stringAsCsv() {
            return String.format("%s;%s;%s;%s", from, to, statusFrom, statusTo);
        }

        @Override
        public int hashCode() {
            return from.toString().hashCode() * 31 + to.hashCode() * 32 + statusFrom.hashCode() * 33 + statusTo.hashCode() * 34;
        }

        @Override
        public boolean equals(Object p) {
            return (p != null)
                    && (p instanceof Period)
                    && ((Period) p).from.equals(this.from)
                    && ((Period) p).to.equals(this.to)
                    && ((Period) p).statusFrom.equals(this.statusFrom)
                    && ((Period) p).statusTo.equals(this.statusTo);
        }
    }

    private static Long getTime(BrooklynApplicationStatus state) {

        //old
        //return state.getObservedAt().getEpochSecond() - state.getBaseTime().getEpochSecond();

        long observed = state.getObservedAt().toEpochMilli();
        long base = state.getBaseTime().toEpochMilli();
        return ((observed - base) / 1000);
    }

    private static void loadBeforeAndErrors(String scenario) throws Exception {


        List<BrooklynApplicationStatus> status = loadAllElementsFromFolder(scenario);

        List<MonkeyTask> monkeyTasks = MonkeyBlueprintLoader.loadTasks(scenario);
        List<String> before = loadBefore(scenario);

        if (before.size() != monkeyTasks.size()) {
            throw new RuntimeException("error in size");
        }


        for (int i = 0; i < monkeyTasks.size(); i++) {
            MonkeyTask monkeyTask = monkeyTasks.get(i);
            String beforeEntry = before.get(i);
            System.out.println(String.format("[%s]-- seconds=%s, entity=%s, op=%s", i, beforeEntry, monkeyTask.getEntity(), monkeyTask.getOperation()));
        }

        int i = 0;
        for (BrooklynApplicationStatus m : status) {
            if (m.getAppStatus().size() == 4) {
                System.out.println("--" + m.getObservedAt() + "--" + i);
            }
            i++;
        }
    }

    private static List<BrooklynApplicationStatus> loadAllElementsFromFolder(String scenario) throws Exception {

        String healthCheckFolder = "monkey/" + scenario + "/healthcheck";

        Stream<Path> paths = Files.walk(Paths.get(healthCheckFolder));
        paths
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .forEach(System.out::println);

        List<Path> pathss = Files.walk(Paths.get(healthCheckFolder))
                .filter(Files::isRegularFile)
                .filter(p -> !p.getFileName().toString().contains("DS_Store"))
                .collect(toList());


        List<BrooklynApplicationStatus> status = new ArrayList<>();
        int i = 0;
        for (Path p : pathss) {
            status.add(mapToStatus(p, i));
            i++;
        }
        return status;

    }

    private static BrooklynApplicationStatus mapToStatus(Path p, int i) {

        Gson gs = new Gson();
        try {
            BrooklynApplicationStatus s = gs.fromJson(new JsonReader(new FileReader(p.toFile().toString())), BrooklynApplicationStatus.class);

            System.out.println("[" + i + "]--------------------------------- " + p.getFileName());
            System.out.println("seconds =>" + (s.getObservedAt().getEpochSecond() - s.getBaseTime().getEpochSecond()));
            System.out.printf("running=> " + s.getAppStatus().entrySet().stream()
                    .filter(e -> !e.getKey().equalsIgnoreCase("softcareapp"))
                    .filter(e -> e.getValue().equalsIgnoreCase("running"))
                    .count() + "\n");

            //s.setBaseTime(null);
            //s.setObservedAt(null);

            s.getAppStatus().remove("SoftcareApp");

            System.out.println(s.toStringAsJson());
            return s;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    //------


    public static List<String> loadBefore(String folder) throws IOException {
        List<String> before = Files.readAllLines(Paths.get("monkey/" + folder + "/reports/before.csv"));
        before.remove(0);

        return before;
    }

    private static List<MonkeyTask> mapToMigrationTasks(Path p) {
        Gson gs = new Gson();
        try {
            MonkeyBluePrint f = (MonkeyBluePrint) gs.fromJson(new JsonReader(new FileReader(p.toFile().toString())), MonkeyBluePrint.class);
            return f.getTasks();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static LinkedList<Integer> injectionsDuringRecovery() {
        //Integer[] e = {1, 9, 12, 16, 24, 25, 27, 35, 36, 37, 38, 39, 40, 43, 44, 45, 46, 48, 49, 52, 53, 55, 56, 57, 62, 69, 71, 72, 73, 74, 75, 79, 80, 83,
        //        85, 89, 91, 95, 96, 97, 100, 102, 104, 112, 116, 118, 119, 120, 123, 133, 138, 145, 149, 154, 156, 158, 164, 165, 166, 167};

        Integer[] e = {
                6, 53, 66, 91, 136, 143, 150,
                195, 199, 207, 213, 216,
                223, 238, 240, 248, 251, 262,
                267, 286, 291, 301, 305, 307,
                336, 375, 388, 393, 395, 404,
                406, 430, 435, 449, 461, 480,
                491, 515, 518, 521, 538, 547,
                559, 599, 619, 629, 633, 637,
                654, 706, 728, 764, 789, 816,
                828, 839, 872, 874, 880, 883};

        LinkedList<Integer> result = new LinkedList<>();
        result.addAll(Arrays.asList(e));
        return result;
    }
}
