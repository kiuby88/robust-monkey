package org.scenic.monkey.deployer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import org.apache.brooklyn.rest.domain.TaskSummary;
import org.scenic.exception.DeploymentException;
import org.scenic.parallel.DeployedApp;
import org.scenic.proxy.DeployerProxy;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

/**
 * Created by Jose on 10/12/19.
 */
public class BrooklynApplicationDeployer {

    DeployerProxy deployerProxy;

    public BrooklynApplicationDeployer(DeployerProxy deployer) {
        this.deployerProxy = deployer;
    }

    public String deployAndWait(String path) throws Exception {
        System.out.println("Deploying application...");
        String appId = deployApplication(path).getAppId();

        System.out.println("Wainting to deploy");
        waitApplicationIsUp(appId);
        return appId;
    }

    public DeployedApp deployApplication(String path) throws Exception {
        boolean deployed = false;
        int limit = 0;
        DeployedApp app = null;
        while (!deployed && limit < 10) {
            try {
                System.out.println(String.format("[%s] Deploy application app = %s", limit, path));
                app = tryDeployApplication(path);
                System.out.println("--Deployed application appPath =" + path);
                System.out.println("--Checking =" + app.getAppId());
                //FutureTask<Boolean> t = deployerProxy.applicationIsUp(app.getAppId());
                //t.run();
                //t.get()
                deployed = waitApplicationIsUp(app.getAppId());
                System.out.println(String.format("--Checked that app %s is deployed %s =", app.getAppId(), deployed));
            } catch (Exception e) {
                limit++;
            }
        }
        return app;
    }


    private DeployedApp tryDeployApplication(String path) throws Exception {

        String yml = getBluePrint(path);

        String appId = deployApplicationAndWait(yml);

        DeployedApp deployedApp = new DeployedApp();
        deployedApp.setAppId(appId);
        deployedApp.setBrooklynBlueprint(yml);
        return deployedApp;
    }

    public boolean waitApplicationWillBeUp(String applicationId) {
        try {
            FutureTask<Boolean> isUpTask = deployerProxy.applicationWillBeUp(applicationId);
            System.out.println("Running taks");
            isUpTask.run();
            return isUpTask.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new DeploymentException("Application was not started error: " +
                    e.getCause(), applicationId);
        }
    }

    private String deployApplicationAndWait(String yaml) {
        String applicationId = createDeploymentTask(yaml);
        try {
            waitApplicationIsUp(applicationId);
            return applicationId;
        } catch (Exception e) {
            System.out.println("Error waiting application up in brooklyn");
            //Clean application.
            throw e;
        }
    }

    private String createDeploymentTask(String yml) {
        try {
            TaskSummary startTask = deployerProxy.deployApplicationFromYaml(yml);
            return startTask.getEntityId();
        } catch (Exception e) {
            throw new RuntimeException("Error creating deployment tasks");
        }
    }

    public boolean waitApplicationIsUp(String applicationId) {
        try {
            FutureTask<Boolean> isUpTask = deployerProxy.applicationIsUp(applicationId);
            System.out.println("Running taks");
            isUpTask.run();
            return isUpTask.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new DeploymentException("Application was not started error: " +
                    e.getCause(), applicationId);
        }
    }


    private String getBluePrint(String appPath) throws URISyntaxException, IOException {
        URL blueprint = getClass().getClassLoader().getResource(appPath);
        return Resources.toString(blueprint, Charsets.UTF_8);
    }


}
