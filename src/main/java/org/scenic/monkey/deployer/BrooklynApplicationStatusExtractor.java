package org.scenic.monkey.deployer;

import static java.time.Instant.now;

import java.time.Instant;
import java.util.List;

import org.apache.brooklyn.rest.domain.EntitySummary;
import org.scenic.monkey.dto.BrooklynApplicationStatus;
import org.scenic.proxy.DeployerProxy;

/**
 * Created by Jose on 11/12/19.
 */
public class BrooklynApplicationStatusExtractor {


    private DeployerProxy deployerProxy;

    public BrooklynApplicationStatusExtractor(DeployerProxy deployerProxy) {
        this.deployerProxy = deployerProxy;
    }

    public BrooklynApplicationStatus getStatus(String applicationId, Instant baseTime) {

        List<EntitySummary> j = deployerProxy.getDescendants(applicationId);

        BrooklynApplicationStatus appStatus = new BrooklynApplicationStatus()
                .withObservedAt(now())
                .withBaseTime(baseTime)
                ;

        j.parallelStream()
                .forEach(e -> appStatus.addEntityStatus(e.getName(), deployerProxy.getStatus(applicationId, e.getId())));

        return appStatus;
    }
}
