package org.scenic;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.scenic.experiment.Experiment;
import org.scenic.experiment.ExperimentExecutionTrace;
import org.scenic.printer.BoxPlotPrinter;
import org.scenic.printer.CvsPrinter;
import org.scenic.printer.ExperimentSorter;


/**
 * Created by Jose on 17/05/18.
 */
public class CsvReporter {

    public static final String EXTENSION = ".csv";

    public static void main(String[] args) throws IOException {

        //Experiment ex = Experiment.S1A1;

        for (Experiment ex : Experiment.values()) {
            printSort(ex);

            Files.copy(getLocation(ex, ""), getTargetLocation(ex, ""), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(getLocation(ex, "-box"), getTargetLocation(ex, "-box"), StandardCopyOption.REPLACE_EXISTING);
        }

    }

    private static Path getLocation(Experiment ex, String sufix) {
        return Paths.get(ex.name() + "-REPORT/" + ex.name() + "-report" + sufix + EXTENSION);
    }

    private static Path getTargetLocation(Experiment ex, String sufix) {
        return Paths.get("/Users/Jose/papers/CLOUD19/evaluation/data/" + ex.name() + "-report" + sufix + EXTENSION);
    }

    private static void printSort(Experiment ex) throws IOException {
        List<ExperimentExecutionTrace> experimentExecutionTraces = new ExperimentSorter().t(ex);


        CvsPrinter printer = new CvsPrinter();
        printer.print(experimentExecutionTraces);

        BoxPlotPrinter boxPrinter = new BoxPlotPrinter();
        boxPrinter.print(experimentExecutionTraces);
    }


}
