package org.scenic;

import org.scenic.experiment.Experiment;
import org.scenic.proxy.DeployerProxy;

/**
 * Created by Jose on 15/02/19.
 */
public class Evaluation {


    public static void main(String[] args) throws Exception {

        DeployAndMigrateManager manager = new DeployAndMigrateManager(DeployerProxy.newInstance());


        for (int i = 1; i < 6; i++) {
            manager.deployAndMigrate(Experiment.S5, i);
        }

    }


}


