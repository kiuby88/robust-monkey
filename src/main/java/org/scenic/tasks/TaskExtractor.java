package org.scenic.tasks;

import org.apache.brooklyn.rest.domain.TaskSummary;
import org.apache.brooklyn.util.time.Duration;
import org.scenic.proxy.DeployerProxy;


/**
 * Created by Jose on 08/04/18.
 */
public class TaskExtractor {


    private final DeployerProxy proxy;

    private String taskId;

    public TaskExtractor(DeployerProxy proxy
                         //, String taskId
    ) {
        this.proxy = proxy;
        //this.taskId = taskId;
    }


    /*public MigrationTasks extract() {

        TaskSummary t = proxy.getTask(taskId);
        return new MigrationTasks(extract(t));
    }

    public TaskErrors extractDeploymentErrors() {
        TaskSummary t = proxy.getTask(taskId);
        return new TaskErrors(KindOfError.DEPLOY, extractFails(t));
    }

    public TaskErrors extractMigrationErrors() {
        TaskSummary t = proxy.getTask(taskId);
        return new TaskErrors(KindOfError.MIGRATION, extractFails(t));
    }*/

    public TaskDescription extract(TaskSummary t) {

        TaskDescription taskDescription = null;
        System.out.println("Extractig tasks " + t.getDisplayName());
        if (
                (!t.getDisplayName().contains("#")
                        && !t.getDisplayName().contains("(parallel)")
                        //&& ! t.getDisplayName().contains("migra")
                )
                        || (t.getDisplayName().contains("release task #"))
                ) {

            taskDescription = new TaskDescription();
            taskDescription.setEntityName(t.getEntityDisplayName());
            taskDescription.setStartTime(t.getStartTimeUtc());
            taskDescription.setEndTime(t.getEndTimeUtc());
            taskDescription.setTaskName(t.getDisplayName());
            taskDescription.setTaskId(t.getId());

            taskDescription.setDuration(Duration
                    .millis(t.getEndTimeUtc() - t.getStartTimeUtc()).toMilliseconds());


            /*if (!t.getDisplayName().contains("migra")) {
                return MutableList.of(d);
            }*/
        }

        //List<TaskDescription> taskDescriptions = MutableList.of();
        //taskDescriptions.add(d);

        //Extracting the children during migration
        //for (LinkWithMetadata link : t.getChildren()) {
        //    taskDescriptions.addAll(extract(proxy.getTask((String) link.getMetadata().get("id"))));
        //}

        return taskDescription;
    }


    /*private List<TaskErrorDescription> extractFails(TaskSummary t) {


        TaskErrorDescription d = null;
        if (
                (!t.getDisplayName().contains("#")
                        && !t.getDisplayName().contains("(parallel)")
                        && (t.isError() || t.isCancelled())
                        //&& ! t.getDisplayName().contains("migra")
                )
            //Necesito traer todos los datos de los errores, incluso release
            //|| (t.getDisplayName().contains("release task #"))
                ) {

            d = new TaskErrorDescription();

            d.setEntityName(t.getEntityDisplayName());
            d.setStartTime(t.getStartTimeUtc());
            d.setEndTime(t.getEndTimeUtc());
            d.setTaskName(t.getDisplayName());
            d.setTaskId(t.getId());
            d.setError(t.isError());
            d.setCancell(t.isError());
            d.setCurrentStatus(t.getCurrentStatus());
            d.setDetailedStatus(t.getDetailedStatus());


            d.setDuration(Duration
                    .millis(t.getEndTimeUtc() - t.getStartTimeUtc()).toMilliseconds());

            //return MutableList.of(d);
        }

        List<TaskErrorDescription> taskDescriptions = MutableList.of();

        for (LinkWithMetadata link : t.getChildren()) {
            String ss = (String) link.getMetadata().get("id");
            try {
                TaskSummary tt = proxy.getTask((String) link.getMetadata().get("id"));
                taskDescriptions.addAll(extractFails(tt));
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        if (d != null) {
            taskDescriptions.add(d);
        }

        return taskDescriptions;
    }*/


}
