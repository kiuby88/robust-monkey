package org.scenic.experiment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jose on 16/05/18.
 */
public class ExperimentStepsFactory {


    public List<String> experimentSteps(Experiment exp) {


        switch (exp) {
            case S1:
                return getS1();
            case S2:
            case S3:
            case S5:
                return getS2();
            case S4:
                return getS4();
        }
        throw new RuntimeException("Error Devolviendo index para #" + exp.name());
    }


    private List<String> getS1() {
        List<String> l = new ArrayList<>();
        l.add("Forum#release");
        l.add("Forum#start");
        return l;
    }

    private List<String> getS2() {
        List<String> l = new ArrayList<>();
        l.add("Dashboard#stop");
        l.add("Forum#release");
        l.add("Forum#start");
        l.add("Dashboard#restart");
        return l;
    }


    private List<String> getS4() {
        List<String> l = new ArrayList<>();
        l.add("Dashboard#stop");
        l.add("Forum#release");
        l.add("Multimedia#release");
        l.add("Forum#start");
        l.add("Multimedia#start");
        l.add("Dashboard#restart");
        return l;
    }


}
