package org.scenic.experiment;

import java.util.ArrayList;
import java.util.List;

import org.scenic.experiment.Experiment;
import org.scenic.tasks.TaskDescription;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Jose on 15/02/19.
 */
public class ExperimentExecutionTrace {

    private final Experiment experiment;

    private String applicationId;
    private int offset;

    private List<TaskDescription> taskDescriptions;

    public ExperimentExecutionTrace(Experiment experiment) {
        this.experiment = experiment;
        this.taskDescriptions = new ArrayList<>();
    }


    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }


    public List<TaskDescription> getTaskDescriptions() {
        return taskDescriptions;
    }

    public void addTaskDescriptions(TaskDescription taskDescription) {
        this.taskDescriptions.add(taskDescription);
    }

    public Experiment getExperiment() {
        return experiment;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

}
