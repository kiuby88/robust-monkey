package org.scenic.experiment;

/**
 * Created by Jose on 08/04/18.
 */
public enum Experiment {
    S1(false, "S4"),
    S2(false, "S4"),
    S3(false, "S4"),
    S4(false, "S4"),
    S5(false, "S4"),
    SEQ_S4(false, "SEQ_S4"),

    PARALLEL_S4(true, "S4"),
    PARALLEL_S5(true, "S5"),
    PARALLEL_S6(true, "S6"),
    PARALLEL_S7(true, "S7"),
    PARALLEL_S8(true, "S8"),

    R_SC1(true, "R_SC1"),
    R_SC2(true, "R_SC2"),
    R_SC3(true, "R_SC3"),
    R_SC4(true, "R_SC4"),
    R_SC5(true, "R_SC5"),


    FULL_R_SC2(true, "FULL_R_SC2");



    boolean parallel;
    String name;


    Experiment(boolean parallel, String name) {
        this.parallel = parallel;
        this.name = name;
    }

    boolean isParallel() {
        return parallel;
    }

    String getName() {
        return name;
    }
}
