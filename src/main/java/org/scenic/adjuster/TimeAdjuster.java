package org.scenic.adjuster;


import org.scenic.experiment.ExperimentExecutionTrace;
import org.scenic.tasks.TaskDescription;

/**
 * Created by Jose on 17/05/18.
 */
public class TimeAdjuster {


    public ExperimentExecutionTrace adjust(ExperimentExecutionTrace experimentExecutionTrace) {

        long previousTime = 0;
        for (TaskDescription taskDescription : experimentExecutionTrace.getTaskDescriptions()) {
            taskDescription.addDurationIncrement(previousTime);
            previousTime = taskDescription.getDuration();
        }
        return experimentExecutionTrace;
    }

}
