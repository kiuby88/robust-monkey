package org.scenic.printer;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.scenic.experiment.ExperimentExecutionTrace;

/**
 * Created by Jose on 08/04/18.
 */
public class ExecutionTaskPrinter {


    public void createFileReports(ExperimentExecutionTrace execution) throws IOException {

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String dateString = dateFormat.format(date);

        String scenarioName = execution.getExperiment().name();
        boolean folder = new File(execution.getExperiment().name()).mkdir();

        PrintWriter writer = new PrintWriter(scenarioName + File.separator + scenarioName + "-" + execution.getOffset() + "-" + dateString + ".json");
        writer.println(execution.toString());
        writer.close();


        //
//        String rootDirectoryPath = deploymentTrace.getApplicationName();
//        boolean created = new File(rootDirectoryPath).mkdir();
//
//        DefaultApplicationDeploymentReporter reporter =
//                new DefaultApplicationDeploymentReporter(deploymentTrace);
//
//        DefaultApplicationDeploymentReport report = reporter.report();
//        DefaultApplicationDeploymentReportCsvPrinter printer =
//                new DefaultApplicationDeploymentReportCsvPrinter(report);
//
//        String tracesPath = rootDirectoryPath + File.separator + "applicationTraces";
//        created = new File(tracesPath).mkdir();
//
//        for (int i = 0; i < deploymentTrace.getApplicationTraces().size(); i++) {
//            PrintWriter writer = new PrintWriter(tracesPath + File.separator + "trace" + (i + 1) + ".json");
//            writer.println(deploymentTrace.getApplicationTraces().get(i).toJson());
//            writer.close();
//        }
//
//        String pathEvaluation = rootDirectoryPath + File.separator + reportFileName + CSV_EXTENSION;
//        PrintWriter writer = new PrintWriter(pathEvaluation);
//        writer.println(printer.print());
//        writer.close();
    }

}
