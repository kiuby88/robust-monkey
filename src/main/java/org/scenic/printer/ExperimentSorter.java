package org.scenic.printer;


import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.scenic.adjuster.TimeAdjuster;
import org.scenic.experiment.Experiment;
import org.scenic.experiment.ExperimentExecutionTrace;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

/**
 * Created by Jose on 17/05/18.
 */
public class ExperimentSorter {

    public List<ExperimentExecutionTrace> t(Experiment e) throws IOException {

        //final MigrationTaskSorter sorter = new MigrationTaskSorter();

        Stream<Path> paths = Files.walk(Paths.get(e.name()));
        paths
                .filter(Files::isRegularFile)
                .map(Path::toFile)

                .forEach(System.out::println);

        return Files.walk(Paths.get(e.name())).filter(Files::isRegularFile)
                .filter(p -> !p.getFileName().toString().contains("DS_Store"))
                .map(this::mapToMigrationTasks)
                .map(this::adjust)
                .collect(Collectors.toList());
    }

    private ExperimentExecutionTrace adjust(ExperimentExecutionTrace experimentExecutionTrace) {
        return new TimeAdjuster().adjust(experimentExecutionTrace);
    }

    private ExperimentExecutionTrace mapToMigrationTasks(Path p) {
        Gson gs = new Gson();
        try {
            return gs.fromJson(new JsonReader(new FileReader(p.toFile().toString())), ExperimentExecutionTrace.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
