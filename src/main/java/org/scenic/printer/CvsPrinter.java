package org.scenic.printer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.scenic.experiment.Experiment;
import org.scenic.experiment.ExperimentExecutionTrace;
import org.scenic.experiment.ExperimentStepsFactory;

/**
 * Created by Jose on 17/05/18.
 */
public class CvsPrinter {

    public static final String EXTENSION = ".csv";


    public void print(List<ExperimentExecutionTrace> tasks) throws FileNotFoundException {
        //checkExperiment(tasks);
        ExperimentStepsFactory factory = new ExperimentStepsFactory();

        Experiment ex = tasks.get(0).getExperiment();

        //modify if necessary
        List<String> stepDescription = factory.experimentSteps(ex);
        //index.remove(0);

        generateReport(ex, stepDescription, tasks);
    }

    protected void generateReport(Experiment es, List<String> stepDescription, List<ExperimentExecutionTrace> tasks) throws FileNotFoundException {
        List<String> data = new ArrayList<>();
        data.add(String.join(";", stepDescription));
        for (ExperimentExecutionTrace experimentExecutionTrace : tasks) {


            List<String> endTimes = experimentExecutionTrace.getTaskDescriptions().stream()
                    .map(t -> convertToString(t.getDuration()))
                    .collect(Collectors.toList());

            data.add(String.join(";", endTimes));
        }
        printReport(es, data);
    }

    /*private List<String> modifyName(List<String> index) {
       return index.stream()
               .map(s -> s.replace("SoftcareApp", "SeaClouds"))
               .map(s -> s.replace("SeaClouds#release task # ", "SeaClouds#release-"))
               .map(s -> s.replace("seaclouds-", ""))
               .map(s -> s.replace("restart", "re-start"))
               .map(s -> s.replace("dashboard", "Dashboard"))
               .map(s -> s.replace("grafana", "MonitorDashboard"))
               .map(s -> s.replace("planner", "Planner"))
               .map(s -> s.replace("sla-service", "SLAService"))
               .map(s -> s.replace("deployer", "Deployer"))
               .map(s -> s.replace("discoverer", "Discoverer"))
               .map(s -> s.replace("restart", "re-start"))
               .map(s -> s.replace("monitor-manager", "MonitorManager"))
               .map(s -> s.replace("data-collector", "DataCollector"))
               //este es el que modifica el release
               .map(this::releaseMap)
               .collect(Collectors.toList());
    }*/

    private String releaseMap(String taskName){
        if(taskName.contains("SeaClouds#release-")) {
            return taskName.replace("SeaClouds#release-","") +"#release";
        }
        return taskName;
    }


    protected String  convertToString(Long l){
        return String.valueOf(l.doubleValue()
                /1000d
        );

    }


    /*private void checkExperiment(List<SortedMigrationTasks> tasks) {
        Experiment es = tasks.get(0).getScenario();
        int s = tasks.stream().filter(t -> t.getScenario() != es).collect(Collectors.toList()).size();
        if (s != 0) {
            throw new RuntimeException("Experiments are not the same for " + es);
        }
    }*/


    protected void printReport(Experiment experiment, List<String> data) throws FileNotFoundException {

        String folder = experiment.name() +"-REPORT";
         new File(folder).mkdir();

        PrintWriter writer = new PrintWriter(folder + File.separator + experiment.name() + getSuffix() + EXTENSION);
        for (String row : data) {
            writer.println(row);
        }
        writer.close();

    }


    protected String getSuffix() {
        return "-report";
    }
}
