package org.scenic.printer;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.scenic.experiment.Experiment;
import org.scenic.experiment.ExperimentExecutionTrace;


/**
 * Created by Jose on 16/06/18.
 */
public class BoxPlotPrinter extends CvsPrinter {


    @Override
    protected void generateReport(Experiment es, List<String> index, List<ExperimentExecutionTrace> tasks) throws FileNotFoundException {
        List<String> data = new ArrayList<>();
        data.add("task;value");
        for (ExperimentExecutionTrace task : tasks) {
            data.addAll(getSubTaskList(index, task));
        }
        System.out.println();
        printReport(es, data);
    }

    private List<String> getSubTaskList(final List<String> index, ExperimentExecutionTrace experimentExecutionTrace) {

        List<String> subTasks = new ArrayList<>();
        for (int i = 0; i < index.size(); i++) {
            List<String> row = new ArrayList<>();
            row.add(index.get(i));
            //String endTime = convertToString(experimentExecutionTrace.getTaskDescriptions().get(i).getEndTime());
            String endTime = convertToString(experimentExecutionTrace.getTaskDescriptions().get(i).getDuration());
            row.add(endTime);
            subTasks.add(String.join(";", row));
        }
        return subTasks;
    }

    protected String getSuffix() {
        return "-report-box";
    }


}
