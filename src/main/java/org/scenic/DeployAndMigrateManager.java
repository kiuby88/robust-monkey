package org.scenic;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import org.apache.brooklyn.rest.domain.TaskSummary;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import org.scenic.exception.DeploymentException;
import org.scenic.experiment.Experiment;
import org.scenic.experiment.ExperimentExecutionTrace;
import org.scenic.plan.Plan;
import org.scenic.plan.Step;
import org.scenic.printer.ExecutionTaskPrinter;
import org.scenic.proxy.DeployerProxy;
import org.scenic.plan.PlanStepExecutor;


/**
 * Created by Jose on 08/04/18.
 */
public class DeployAndMigrateManager {


    private final DeployerProxy proxy;
    private final PlanStepExecutor planStepExecutor;

    public DeployAndMigrateManager(DeployerProxy proxy) {
        this.proxy = proxy;
        planStepExecutor = new PlanStepExecutor();
    }


    public ExperimentExecutionTrace deployAndMigrate(Experiment experiment, int i) throws Exception {

        String applicationId = "";

        String yml = getBluePrint(experiment.name().toLowerCase());

        //MigrationTasks migrationTasks = null;
        ExperimentExecutionTrace experimentExecutionTrace = null;
        boolean done = false;
        boolean deployed = false;
        int limit = 0;
        while ((!done) || (limit > 10)) {
            try {

                experimentExecutionTrace = new ExperimentExecutionTrace(experiment);
                System.out.println("Start to deploy " + experiment.name() + "#" + i);

                applicationId = deployApp(yml);
                experimentExecutionTrace.setApplicationId(applicationId);
                experimentExecutionTrace.setOffset(i);


                System.out.println("Deployed " + experiment.name() + "#" + i);
                //Start plan execution
                System.out.println("Executing plan " + experiment.name() + "#" + i);
                Plan plan = getPlan(experiment);


                for (Step step : plan.getSteps()) {
                    planStepExecutor.execute(experimentExecutionTrace, step);
                }

                new ExecutionTaskPrinter().createFileReports(experimentExecutionTrace);

                done = true;
            } catch (DeploymentException e) {
                limit++;
                clean(applicationId);
            } finally {
                clean(applicationId);
                waitIntervalBetweenDeployments();
            }
        }
        return experimentExecutionTrace;
    }




    private void clean(String applicationId) {
        proxy.deleteApplication(applicationId);
        //SoftlayerCleaner.clean();

    }

    private String deployApp(String yml) throws Exception {
        TaskSummary startTask = proxy.deployApplicationFromYaml(yml);
        try {
            waitApplicationIsUp(startTask.getEntityId());
            return startTask.getEntityId();
        } catch (DeploymentException e) {
            e.setTaskId(startTask.getId());
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    /*private MigrationTasks migrateApplication(Experiment experiment, String appId) throws Exception {
        String params = getMigrationParams(experiment.name().toLowerCase());

        TaskSummary t = proxy.migrateApplication(appId, params);

        if (t.isError()) {
            throw new MigrateException("Error in migration. Task return an error", appId, t.getId());
        }

        //MigrationTasks tE = new TaskExtractor(proxy, "VNyqavEB").extract();
        MigrationTasks tE = new TaskExtractor(proxy, t.getId()).extract();

        tE.setTasks(tE.getTasks().stream().filter(p -> p != null).collect(Collectors.toList()));
        return tE;
    }*/


    private String getBluePrint(String name) throws URISyntaxException, IOException {
        URL blueprint = getClass().getClassLoader().getResource(name + "/" + name + ".yml");
        return Resources.toString(blueprint, Charsets.UTF_8);
    }

    private Plan getPlan(Experiment experiment) throws URISyntaxException, IOException {
        URL blueprint = getClass().getClassLoader().getResource(experiment + "/plan-" + experiment + ".json");
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(Resources.toString(blueprint, Charsets.UTF_8), Plan.class);
    }


    private String getMigrationParams(String name) throws URISyntaxException, IOException {
        URL blueprint = getClass().getClassLoader().getResource(name + "/" + name + "-params.txt");
        return Resources.toString(blueprint, Charsets.UTF_8);
    }

    private void waitIntervalBetweenDeployments() {
        try {
            Thread.sleep(120000);
        } catch (InterruptedException e) {
            throw new IllegalStateException("Error waiting between deployments");
        }
    }


    private void waitApplicationIsUp(String applicationId) {
        try {
            FutureTask<Boolean> isUpTask = proxy.applicationIsUp(applicationId);
            isUpTask.run();
            isUpTask.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new DeploymentException("Application was not started error: " +
                    e.getCause(), applicationId);
        }
    }

    /*
    private void cleanPaas(){
        CfCleaner cleaner = new CfCleaner();
        cleaner.cleanAll();
    }*/

}
