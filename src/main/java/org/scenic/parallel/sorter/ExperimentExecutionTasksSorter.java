package org.scenic.parallel.sorter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.scenic.parallel.extraction.ExperimentInstanceTasks;
import org.scenic.parallel.extraction.TaskDescription;

/**
 * Created by Jose on 23/10/19.
 */
public class ExperimentExecutionTasksSorter {


    public SortedExecutionTasks sort(ExperimentInstanceTasks experimentInstanceTasks) {
        List<String> index = new SorterIndexFactory().getSorterIndex(experimentInstanceTasks.getScenario());
        Map<String, TaskDescription> map = getMap(experimentInstanceTasks);

        List<TaskDescription> sortedTaskDescription = new ArrayList<>();
        for (String ind : index) {
            sortedTaskDescription.add(map.get(ind));
        }

        SortedExecutionTasks sortedMigrationTask = copySortedMigrationTask(experimentInstanceTasks);
        sortedMigrationTask.setTasks(sortedTaskDescription);
        return sortedMigrationTask;
    }


    public Map<String, TaskDescription> getMap(ExperimentInstanceTasks experimentInstanceTasks) {
        Map<String, TaskDescription> map = new HashMap<>();
        for (TaskDescription des : experimentInstanceTasks.getTasks()) {
            map.put(getKey(des), des);
        }
        return map;
    }

    private String getKey(TaskDescription description) {
        return description.getEntityName() + "#" + description.getTaskName();
    }

    private SortedExecutionTasks copySortedMigrationTask(ExperimentInstanceTasks experimentInstanceTasks) {
        SortedExecutionTasks sortedMigrationTasks = new SortedExecutionTasks();
        sortedMigrationTasks.setOffset(experimentInstanceTasks.getOffset());
        sortedMigrationTasks.setScenario(experimentInstanceTasks.getScenario());
        return sortedMigrationTasks;
    }

}
