package org.scenic.parallel.sorter;

import java.util.List;

import org.scenic.experiment.Experiment;
import org.scenic.parallel.extraction.TaskDescription;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Jose on 23/10/19.
 */
public class SortedExecutionTasks {

    private int offset;

    private Experiment scenario;

    private List<TaskDescription> tasks;

    public SortedExecutionTasks(){

    }

    public List<TaskDescription> getTasks() {
        return tasks;
    }

    public int getOffset() {
        return offset;
    }

    public Experiment getScenario() {
        return scenario;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void setScenario(Experiment scenario) {
        this.scenario = scenario;
    }

    public void setTasks(List<TaskDescription> tasks) {
        this.tasks = tasks;
    }

    @Override
    public String toString(){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
