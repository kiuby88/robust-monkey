package org.scenic.parallel.sorter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.scenic.experiment.Experiment;


/**
 * Created by Jose on 16/05/18.
 */
public class SorterIndexFactory {

    public List<String> getSorterIndex(Experiment exp) {


        switch (exp) {
            case PARALLEL_S4:
            case SEQ_S4:
                return getPARALLEL_S4();
            case PARALLEL_S5:
                return getPARALLEL_S5();
            case PARALLEL_S6:
                return getPARALLEL_S6();
            case R_SC1:
                return getR_SC1();

            case R_SC2:
            case FULL_R_SC2:
                return getR_SC2();

            case R_SC3:
                return getR_SC3();
            case R_SC4:
                return getR_SC4();
            case R_SC5:
                return getR_SC5();
        }
        throw new RuntimeException("Error Devolviendo index para #" + exp.name());
    }

    public Function<String, String> getMapperTasks(Experiment exp) {


        switch (exp) {
            case PARALLEL_S4:
            case SEQ_S4:
                return getMapperPARALLEL_S4();
            case PARALLEL_S5:
                return getMapperPARALLEL_S5();
            case PARALLEL_S6:
                return getMapperPARALLEL_S6();
            case R_SC1:
                return getMapperR_SC1();

            case R_SC2:
            case FULL_R_SC2:
                return getMapperR_SC2();

            case R_SC3:
                return getMapperR_SC3();
            case R_SC4:
                return getMapperR_SC4();
            case R_SC5:
                return getMapperR_SC5();
        }
        throw new RuntimeException("Error Devolviendo mapping para #" + exp.name());
    }

    private Function<String, String> getMapperPARALLEL_S4() {
        return t -> {
            if ("Softcare_dashboard#stop".equalsIgnoreCase(t)) {
                return "Dashboard#stop";
            }
            if ("Softcare_dashboard#restart".equalsIgnoreCase(t)) {
                return "Dashboard#restart";
            }
            if ("SoftcareWS#stop".equalsIgnoreCase(t)) {
                return "SoftcareWS#release";
            }
            if ("Forum#stop".equalsIgnoreCase(t)) {
                return "Forum#release";
            }
            return t;
        };
    }

private Function<String, String> getMapperR_SC1() {
        return t -> {
            if ("Softcare_dashboard#stop".equalsIgnoreCase(t)) {
                return "Dashboard#stop";
            }
            if ("Softcare_dashboard#restart".equalsIgnoreCase(t)) {
                return "Dashboard#restart";
            }
            if ("SoftcareDB#stop".equalsIgnoreCase(t)) {
                return "SoftcareDB#release";
            }
            if ("ForumDB#stop".equalsIgnoreCase(t)) {
                return "ForumDB#release";
            }
            return t;
        };
    }

    private Function<String, String> getMapperR_SC2() {
        return t -> {
            if ("Softcare_dashboard#stop".equalsIgnoreCase(t)) {
                return "Dashboard#stop";
            }
            if ("Softcare_dashboard#restart".equalsIgnoreCase(t)) {
                return "Dashboard#restart";
            }
            return t;
        };
    }

    private Function<String, String> getMapperR_SC3() {
        return t -> {
            if ("Softcare_dashboard#stop".equalsIgnoreCase(t)) {
                return "Dashboard#stop";
            }
            if ("Softcare_dashboard#restart".equalsIgnoreCase(t)) {
                return "Dashboard#restart";
            }
            if ("Forum#stop".equalsIgnoreCase(t)) {
                return "Forum#release";
            }

            return t;
        };
    }
    private Function<String, String> getMapperR_SC4() {
        return t -> {
            if ("Softcare_dashboard#stop".equalsIgnoreCase(t)) {
                return "Dashboard#stop";
            }
            if ("Softcare_dashboard#restart".equalsIgnoreCase(t)) {
                return "Dashboard#restart";
            }
            if ("SoftcareWS#stop".equalsIgnoreCase(t)) {
                return "SoftcareWS#release";
            }

            return t;
        };
    }

    private Function<String, String> getMapperR_SC5() {
        return t -> {
            if ("Softcare_dashboard#stop".equalsIgnoreCase(t)) {
                return "Dashboard#stop";
            }
            if ("Softcare_dashboard#restart".equalsIgnoreCase(t)) {
                return "Dashboard#restart";
            }
            if ("Forum#stop".equalsIgnoreCase(t)) {
                return "Forum#release";
            }
            return t;
        };
    }


    private Function<String, String> getMapperPARALLEL_S5() {

        return t -> {
            if ("Softcare_dashboard#stop".equalsIgnoreCase(t)) {
                return "Dashboard#stop";
            }
            if ("Softcare_dashboard#restart".equalsIgnoreCase(t)) {
                return "Dashboard#restart";
            }
            if ("SoftcareWS#stop".equalsIgnoreCase(t)) {
                return "SoftcareWS#release";
            }
            if ("SoftcareWS#start".equalsIgnoreCase(t)) {
                return "SoftcareWS#start";
            }
            if ("Forum#stop".equalsIgnoreCase(t)) {
                return "Forum#stop";
            }
            if ("Forum#restart".equalsIgnoreCase(t)) {
                return "Forum#restart";
            }
            if ("ForumDB#restart".equalsIgnoreCase(t)) {
                return "ForumDB#restart";
            }
            return t;
        };
    }


    private List<String> getPARALLEL_S4() {
        List<String> l = new ArrayList<>();
        l.add("Softcare_dashboard#stop");
        l.add("Forum#stop");
        l.add("SoftcareWS#stop");
        l.add("SoftcareWS#start");
        l.add("Forum#start");
        l.add("Softcare_dashboard#restart");
        return l;
    }

    private List<String> getPARALLEL_S5() {
        List<String> l = new ArrayList<>();
        l.add("Softcare_dashboard#stop");
        l.add("Forum#stop");
        l.add("SoftcareWS#stop");
        l.add("ForumDB#restart");
        l.add("Forum#restart");
        l.add("SoftcareWS#start");
        l.add("Softcare_dashboard#restart");
        return l;
    }

    private List<String> getPARALLEL_S6() {
        List<String> l = new ArrayList<>();
        l.add("Softcare_dashboard#stop");
        l.add("Forum#stop");
        l.add("SoftcareWS#stop");
        l.add("ForumDB#restart");
        l.add("SoftcareWS#start");
        l.add("Forum#restart");
        l.add("SoftcareWS#stop1");
        l.add("SoftcareWS#start1");
        l.add("Softcare_dashboard#restart");
        return l;
    }

    private List<String> getR_SC1() {
        List<String> l = new ArrayList<>();
        l.add("Softcare_dashboard#stop");
        l.add("Forum#stop");
        l.add("SoftcareWS#stop");
        l.add("ForumDB#stop");
        l.add("SoftcareDB#stop");
        l.add("ForumDB#start");
        l.add("SoftcareDB#start");
        l.add("Forum#restart");
        l.add("SoftcareWS#restart");
        l.add("Softcare_dashboard#restart");

        return l;
    }

    private List<String> getR_SC2() {
        List<String> l = new ArrayList<>();
        l.add("Softcare_dashboard#stop");
        l.add("Forum#stop");
        l.add("SoftcareWS#stop");
        l.add("ForumDB#restart");
        l.add("SoftcareDB#restart");
        l.add("Forum#restart");
        l.add("SoftcareWS#restart");
        l.add("Softcare_dashboard#restart");

        return l;
    }


    private List<String> getR_SC3() {
        List<String> l = new ArrayList<>();
        l.add("Softcare_dashboard#stop");
        l.add("Forum#stop");
        l.add("Forum#start");
        l.add("SoftcareWS#restart");
        l.add("Softcare_dashboard#restart");

        return l;
    }

    private List<String> getR_SC4() {
        List<String> l = new ArrayList<>();
        l.add("Softcare_dashboard#stop");
        l.add("SoftcareWS#stop");
        l.add("SoftcareWS#start");
        l.add("Forum#restart");
        l.add("Softcare_dashboard#restart");

        return l;
    }

    private List<String> getR_SC5() {
        List<String> l = new ArrayList<>();
        l.add("Softcare_dashboard#stop");
        l.add("SoftcareDB#restart");
        l.add("SoftcareWS#stop");
        l.add("SoftcareWS#restart");
        l.add("Forum#stop");
        l.add("Forum#start");
        l.add("Softcare_dashboard#restart");
        return l;
    }

    private Function<String, String> getMapperPARALLEL_S6() {
        return t -> {
            if ("Softcare_dashboard#stop".equalsIgnoreCase(t)) {
                return "Dashboard#stop";
            }
            if ("Softcare_dashboard#restart".equalsIgnoreCase(t)) {
                return "Dashboard#restart";
            }
            if ("SoftcareWS#stop".equalsIgnoreCase(t)) {
                return "SoftcareWS#release";
            }
            if ("SoftcareWS#start".equalsIgnoreCase(t)) {
                return "SoftcareWS#start";
            }

            if ("SoftcareWS#stop1".equalsIgnoreCase(t)) {
                return "SoftcareWS#release1";
            }
            if ("SoftcareWS#start1".equalsIgnoreCase(t)) {
                return "SoftcareWS#start1";
            }

            if ("Forum#stop".equalsIgnoreCase(t)) {
                return "Forum#stop";
            }
            if ("Forum#restart".equalsIgnoreCase(t)) {
                return "Forum#restart";
            }
            if ("ForumDB#restart".equalsIgnoreCase(t)) {
                return "ForumDB#restart";
            }
            return t;
        };
    }
}
