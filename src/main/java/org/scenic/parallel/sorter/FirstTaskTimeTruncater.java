package org.scenic.parallel.sorter;

import java.util.List;

import org.scenic.parallel.extraction.TaskDescription;

/**
 * Created by Jose on 23/10/19.
 */
public class FirstTaskTimeTruncater {


    public SortedExecutionTasks truncate(SortedExecutionTasks sMT) {
        SorterIndexFactory factory = new SorterIndexFactory();

        List<String> index = factory.getSorterIndex(sMT.getScenario());

        for (int i = 0; i < index.size(); i++) {

            TaskDescription t = sMT.getTasks().get(i);

            if (!index.get(i).equals(t.getEntityName() + "#" + t.getTaskName())) {
                System.out.println(index.get(i) + " vs " + t.getEntityName() + "#" + t.getTaskName());
                throw new RuntimeException("SortedMigration can not be trucate because not expected order");
            }
        }
        truncate(sMT.getTasks());
        return sMT;
    }

    private void truncate(List<TaskDescription> tasks) {
        TaskDescription first = tasks.get(0);
        Long start = first.getStartTime();
        tasks.stream().forEach(t -> t.setEndTime(t.getEndTime() - start));
        //tasks.remove(0);
    }


}
