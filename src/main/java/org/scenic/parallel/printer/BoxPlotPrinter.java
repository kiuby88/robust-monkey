package org.scenic.parallel.printer;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.scenic.experiment.Experiment;
import org.scenic.parallel.sorter.SortedExecutionTasks;


/**
 * Created by Jose on 16/06/18.
 */
public class BoxPlotPrinter extends CvsPrinter {


    @Override
    protected void generateReport(Experiment es, List<String> index, List<SortedExecutionTasks> tasks) throws FileNotFoundException {
        List<String> data = new ArrayList<>();
        data.add("task;value");
        for (SortedExecutionTasks task : tasks) {
            data.addAll(getSubTaskList(index, task));
        }
        System.out.println();
        printReport(es, data);
    }

    private List<String> getSubTaskList(final List<String> index, SortedExecutionTasks tasks) {

        List<String> subTasks = new ArrayList<>();
        for (int i = 0; i < index.size(); i++) {
            List<String> row = new ArrayList<>();
            row.add(index.get(i));
            String endTime = convertToString(tasks.getTasks().get(i).getEndTime());
            row.add(endTime);
            subTasks.add(String.join(";", row));
        }
        return subTasks;
    }

    protected String getSuffix() {
        return "-report-box";
    }


}
