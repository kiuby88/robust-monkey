package org.scenic.parallel.printer;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


import org.scenic.experiment.Experiment;
import org.scenic.parallel.sorter.SortedExecutionTasks;
import org.scenic.parallel.sorter.SorterIndexFactory;

/**
 * Created by Jose on 17/05/18.
 */
public class CvsPrinter {

    public static final String EXTENSION = ".csv";


    public void print(List<SortedExecutionTasks> tasks) throws FileNotFoundException {
        checkExperiment(tasks);
        SorterIndexFactory factory = new SorterIndexFactory();

        Experiment es = tasks.get(0).getScenario();

        //List<String> index = modifyName(factory.getSorterIndex(es));
        List<String> index = factory.getSorterIndex(es).stream().map(factory.getMapperTasks(es)).collect(toList());


        //index.remove(0);

        generateReport(es, index, tasks);
    }

    protected void generateReport(Experiment es, List<String> index, List<SortedExecutionTasks> tasks) throws FileNotFoundException {
        List<String> data = new ArrayList<>();
        data.add(String.join(";", index));
        for (SortedExecutionTasks task : tasks) {


            List<String> endTimes = task.getTasks().stream().map(t -> convertToString(t.getEndTime())).collect(toList());
            data.add(String.join(";", endTimes));
        }
        printReport(es, data);
    }

    private List<String> modifyName(List<String> index) {

        //Aqui se necesitara un modify por experimento

        return
               index.stream()
               .map(s -> s.replace("SoftcareApp", "SeaClouds"))
               .map(s -> s.replace("SeaClouds#release task # ", "SeaClouds#release-"))
               .map(s -> s.replace("seaclouds-", ""))
               .map(s -> s.replace("restart", "re-start"))
               .map(s -> s.replace("dashboard", "Dashboard"))
               .map(s -> s.replace("grafana", "MonitorDashboard"))
               .map(s -> s.replace("planner", "Planner"))
               .map(s -> s.replace("sla-service", "SLAService"))
               .map(s -> s.replace("deployer", "Deployer"))
               .map(s -> s.replace("discoverer", "Discoverer"))
               .map(s -> s.replace("restart", "re-start"))
               .map(s -> s.replace("monitor-manager", "MonitorManager"))
               .map(s -> s.replace("data-collector", "DataCollector"))

               //este es el que modifica el release
               .map(this::releaseMap)
               .collect(toList());
    }

    private String releaseMap(String taskName){
        if(taskName.contains("SeaClouds#release-")){
            return taskName.replace("SeaClouds#release-","") +"#release";
        }
        return taskName;
    }


    protected String  convertToString(Long l){
        return String.valueOf(l.doubleValue()
                /1000d
        );

    }


    private void checkExperiment(List<SortedExecutionTasks> tasks) {
        Experiment es = tasks.get(0).getScenario();
        int s = tasks.stream().filter(t -> t.getScenario() != es).collect(toList()).size();
        if (s != 0) {
            throw new RuntimeException("Experiments are not the same for " + es);
        }
    }


    protected void printReport(Experiment experiment, List<String> data) throws FileNotFoundException {

        String folder = experiment.name() +"-REPORT";
         new File(folder).mkdir();


        PrintWriter writer = new PrintWriter(folder + File.separator + experiment.name() + getSuffix() + EXTENSION);
        for (String row : data) {
            writer.println(row);
        }
        writer.close();
    }

    protected String getSuffix() {
        return "-report";
    }
}
