package org.scenic.parallel.printer;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.scenic.parallel.extraction.ExperimentInstanceTasks;

/**
 * Created by Jose on 08/04/18.
 */
public class ScenariosTaskPrinter {


    public void createFileReports(ExperimentInstanceTasks mT) throws IOException {

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Date date = new Date();
        String dateString = dateFormat.format(date);

        boolean folder = new File(mT.getScenario().name()).mkdir();

        PrintWriter writer = new PrintWriter(mT.getScenario().name() + File.separator + mT.getScenario().name() + "-" + mT.getOffset() + "-" + dateString + ".json");
        writer.println(mT.toString());
        writer.close();


        //
//        String rootDirectoryPath = deploymentTrace.getApplicationName();
//        boolean created = new File(rootDirectoryPath).mkdir();
//
//        DefaultApplicationDeploymentReporter reporter =
//                new DefaultApplicationDeploymentReporter(deploymentTrace);
//
//        DefaultApplicationDeploymentReport report = reporter.report();
//        DefaultApplicationDeploymentReportCsvPrinter printer =
//                new DefaultApplicationDeploymentReportCsvPrinter(report);
//
//        String tracesPath = rootDirectoryPath + File.separator + "applicationTraces";
//        created = new File(tracesPath).mkdir();
//
//        for (int i = 0; i < deploymentTrace.getApplicationTraces().size(); i++) {
//            PrintWriter writer = new PrintWriter(tracesPath + File.separator + "trace" + (i + 1) + ".json");
//            writer.println(deploymentTrace.getApplicationTraces().get(i).toJson());
//            writer.close();
//        }
//
//        String pathEvaluation = rootDirectoryPath + File.separator + reportFileName + CSV_EXTENSION;
//        PrintWriter writer = new PrintWriter(pathEvaluation);
//        writer.println(printer.print());
//        writer.close();
    }

}
