package org.scenic.parallel;

/**
 * Created by Jose on 13/10/19.
 */
public class DeployedApp {

    private String appId;

    private String brooklynBlueprint;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getBrooklynBlueprint() {
        return brooklynBlueprint;
    }

    public void setBrooklynBlueprint(String brooklynBlueprint) {
        this.brooklynBlueprint = brooklynBlueprint;
    }
}
