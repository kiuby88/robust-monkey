package org.scenic.parallel.main;

import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;

import org.scenic.experiment.Experiment;
import org.scenic.parallel.printer.BoxPlotPrinter;
import org.scenic.parallel.printer.CvsPrinter;
import org.scenic.parallel.sorter.ExperimentSorter;
import org.scenic.parallel.sorter.FirstTaskTimeTruncater;
import org.scenic.parallel.sorter.SortedExecutionTasks;


/**
 * Created by Jose on 17/05/18.
 */
public class ExperimentReporter {

    public static final String EXTENSION = ".csv";

    public static void main(String[] args) throws IOException {

        Experiment ex = Experiment.R_SC4;

        //for (Experiment ex : Experiment.values()) {
            //printSort(ex);
            //CopyOption[] options = new CopyOption[] {StandardCopyOption.REPLACE_EXISTING};
            //Files.copy(getLocation(ex, ""), getTargetLocation(ex, ""), StandardCopyOption.REPLACE_EXISTING);
            //Files.copy(getLocation(ex, "-box"), getTargetLocation(ex, "-box"), StandardCopyOption.REPLACE_EXISTING);
        //}
        createReports(ex);

    }

    private static void createReports(Experiment ex) throws IOException {
        printSort(ex);
        CopyOption[] options = new CopyOption[] {StandardCopyOption.REPLACE_EXISTING};
        Files.copy(getLocation(ex, ""), getTargetLocation(ex, ""), StandardCopyOption.REPLACE_EXISTING);
        Files.copy(getLocation(ex, "-box"), getTargetLocation(ex, "-box"), StandardCopyOption.REPLACE_EXISTING);
    }

    private static Path getLocation(Experiment ex, String sufix){
        return Paths.get(ex.name() + "-REPORT/" + ex.name() +"-report"+sufix+ EXTENSION);
    }

    private static Path getTargetLocation(Experiment ex, String sufix){
        return Paths.get("/Users/Jose/papers/robust-parallel/evaluation/data/"+ex.name()+"-report"+sufix+ EXTENSION);
    }

    private static void printSort(Experiment ex) throws IOException {
        List<SortedExecutionTasks> sortedExecutionTaskss = new ExperimentSorter().t(ex);
        FirstTaskTimeTruncater truncater = new FirstTaskTimeTruncater();

        List<SortedExecutionTasks> truncatedTasks = sortedExecutionTaskss.stream()
                .map(truncater::truncate)
                .collect(Collectors.toList());

        //System.out.println(truncatedTasks);
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

        CvsPrinter printer = new CvsPrinter();
        printer.print(truncatedTasks);
        BoxPlotPrinter boxPrinter = new BoxPlotPrinter();
        boxPrinter.print(truncatedTasks);
    }


}
