package org.scenic.parallel.main;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.stream.Stream;

import org.apache.brooklyn.rest.domain.EntitySummary;
import org.apache.brooklyn.rest.domain.TaskSummary;
import org.scenic.cleaner.entity.EntitiesCleaner;
import org.scenic.cleaner.entity.EntitiesStopper;
import org.scenic.exception.DeploymentException;
import org.scenic.experiment.Experiment;
import org.scenic.parallel.DeployedApp;
import org.scenic.parallel.clients.OrchestratorClient;
import org.scenic.parallel.extraction.ExperimentInstanceTasks;
import org.scenic.parallel.extraction.TaskExtractor;
import org.scenic.parallel.printer.ScenariosTaskPrinter;
import org.scenic.proxy.DeployerProxy;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

/**
 * Created by Jose on 15/02/19.
 */
public class ParallelEvaluation {

    public static void main(String[] args) throws Exception {

        Experiment experiment = Experiment.R_SC3;

        System.out.println("Starting!!");

        DeployerProxy deployer = DeployerProxy.connectDefault();
        EntitiesStopper stoppers = new EntitiesStopper(deployer);


        System.out.println("-- Instant : " + Instant.now().toEpochMilli());

        ParallelEvaluation parallel = new ParallelEvaluation(deployer);

        System.out.println("Deploying application...");
        DeployedApp deployedApp = parallel.deployApplication(experiment);
        String applicationId = deployedApp.getAppId();
        //String applicationId = "k4PrbBp9";

        System.out.println("Wainting to deploy");
        parallel.waitApplicationIsUp(applicationId);

        System.out.println("Sync application...");
        OrchestratorClient orchestratorClient = OrchestratorClient.build();
        orchestratorClient.sycn(applicationId, parallel.getMmBluePrint(experiment.name()));

        execute(Experiment.R_SC3, applicationId, 6, 10,deployer, stoppers, parallel);

        execute(Experiment.R_SC4, applicationId, 0, 10,deployer, stoppers, parallel);

        /*Nodes nodes = parallel.nodesToOperate(experiment);
        for (int i = init; i < end; i++) {

            System.out.println("Cleaning...");

            stoppers.stopEntities(applicationId, nodes);

            Instant fromNow = Instant.now();

            EntitiesCleaner entitiesCleaner = new EntitiesCleaner(deployer);

            System.out.println("Cleaning entities...");
            entitiesCleaner.cleanEntities(applicationId, nodes);


            System.out.println("Waiting to clean in booklyn...");
            parallel.waitEntitiesFail(applicationId, nodes.getAllEntitiesToCrach());
            System.out.println("Cleaned");

            System.out.println("Updating Status in orchstrator...");
            OrchestratorClient.build().update();

            System.out.println("Delete during Revocery");
            entitiesCleaner.cleanDuringRecovery(applicationId, nodes);

            System.out.println("Waiting application in up...");
            parallel.waitApplicationWillBeUp(applicationId);

            System.out.println("Extracting");

            try {
                parallel.extractExperimentInformation(experiment, applicationId, fromNow, i);
            } catch (Exception e) {
                System.out.printf("Error extracting experiment [" + experiment + "] in [ " + i + "]");
            }
            System.out.println("End iteraction: " + i);
        }*/

        System.out.println("evitc all");

        System.out.println("Fin de main");
    }

    private static void execute(Experiment experiment, String applicationId, int init, int end,
                          DeployerProxy deployer,
                          EntitiesStopper stoppers,
                          ParallelEvaluation parallel) throws Exception {

        Nodes nodes = parallel.nodesToOperate(experiment);
        //int init = 6; // otro forum pero sin ningun control sobre la sesion de este codigo
        //int end = 10;
        for (int i = init; i < end; i++) {

            System.out.println("Cleaning...");

            stoppers.stopEntities(applicationId, nodes);

            Instant fromNow = Instant.now();

            EntitiesCleaner entitiesCleaner = new EntitiesCleaner(deployer);

            System.out.println("Cleaning entities...");
            entitiesCleaner.cleanEntities(applicationId, nodes);


            System.out.println("Waiting to clean in booklyn...");
            parallel.waitEntitiesFail(applicationId, nodes.getAllEntitiesToCrach());
            System.out.println("Cleaned");

            System.out.println("Updating Status in orchstrator...");
            OrchestratorClient.build().update();

            System.out.println("Delete during Revocery");
            entitiesCleaner.cleanDuringRecovery(applicationId, nodes);

            System.out.println("Waiting application in up...");
            parallel.waitApplicationWillBeUp(applicationId);

            System.out.println("Extracting");

            try {
                parallel.extractExperimentInformation(experiment, applicationId, fromNow, i);
            } catch (Exception e) {
                System.out.printf("Error extracting experiment [" + experiment + "] in [ " + i + "]");
            }
            System.out.println("End iteraction: " + i);
        }

    }




    private DeployerProxy deployerProxy;

    void extractExperimentInformation(Experiment experiment, String applicationId, Instant fromNow, int offset) {
        System.out.println("**Extracting information from instant: " + fromNow.toEpochMilli());
        //Este es el que tiene que tiene que sacar los datos y guardarlos los ficheros por experiment
        ExperimentInstanceTasks experimentExecutionTasks = new TaskExtractor(deployerProxy, applicationId, fromNow.toEpochMilli()).extract();
        experimentExecutionTasks.setOffset(offset);
        experimentExecutionTasks.setScenario(experiment);

        try {
            new ScenariosTaskPrinter().createFileReports(experimentExecutionTasks);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public ParallelEvaluation(DeployerProxy deployerProxy) {
        this.deployerProxy = deployerProxy;
    }

    private DeployedApp deployApplication(Experiment experiment) throws Exception {
        boolean deployed = false;
        int limit = 0;
        DeployedApp app = null;
        while (!deployed && limit < 10) {
            try {
                System.out.println(String.format("[%s] Deploy application experiment = %s", limit, experiment));
                app = tryDeployApplication(experiment);
                System.out.println("--Deployed application experiment =" + experiment);
                System.out.println("--Checking =" + app.getAppId());
                //FutureTask<Boolean> t = deployerProxy.applicationIsUp(app.getAppId());
                //t.run();
                //t.get()
                deployed = waitApplicationIsUp(app.getAppId());
                System.out.println(String.format("--Checked that app %s is deployed %s =", app.getAppId(), deployed));
            } catch (Exception e) {
                limit++;
            }
        }
        return app;
    }

    private DeployedApp tryDeployApplication(Experiment experiment) throws Exception {

        String yml = getBluePrint(experiment.name());

        String appId = deployApplicationAndWait(yml);

        DeployedApp deployedApp = new DeployedApp();
        deployedApp.setAppId(appId);
        deployedApp.setBrooklynBlueprint(yml);
        return deployedApp;
    }

    private String deployApplicationAndWait(String yaml) {
        String applicationId = createDeploymentTask(yaml);
        try {
            waitApplicationIsUp(applicationId);
            return applicationId;
        } catch (Exception e) {
            System.out.println("Error waiting application up in brooklyn");
            //Clean application.
            throw e;
        }
    }

    private String createDeploymentTask(String yml) {
        try {
            TaskSummary startTask = deployerProxy.deployApplicationFromYaml(yml);
            return startTask.getEntityId();
        } catch (Exception e) {
            throw new RuntimeException("Error creating deployment tasks");
        }
    }

    /*private String deployApp(DeployerProxy proxy, String yml) throws Exception {
        TaskSummary startTask = proxy.deployApplicationFromYaml(yml);
        try {
            waitApplicationIsUp(proxy, startTask.getEntityId());
            return startTask.getEntityId();
        } catch (DeploymentException e) {
            e.setTaskId(startTask.getId());
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }*/

    private boolean waitApplicationIsUp(String applicationId) {
        try {
            FutureTask<Boolean> isUpTask = deployerProxy.applicationIsUp(applicationId);
            System.out.println("Running taks");
            isUpTask.run();
            return isUpTask.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new DeploymentException("Application was not started error: " +
                    e.getCause(), applicationId);
        }
    }

    private boolean waitApplicationWillBeUp(String applicationId) {
        try {
            FutureTask<Boolean> isUpTask = deployerProxy.applicationWillBeUp(applicationId);
            System.out.println("Running taks");
            isUpTask.run();
            return isUpTask.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new DeploymentException("Application was not started error: " +
                    e.getCause(), applicationId);
        }
    }

    private boolean waitEntityIsNotUp(String applicationId, String entityId) {
        try {
            FutureTask<Boolean> isUpTask = deployerProxy.isNotUp(applicationId, entityId);
            isUpTask.run();
            boolean result = isUpTask.get();
            System.out.println(entityId + ": Is not up ? => " + result);
            return result;
        } catch (InterruptedException | ExecutionException e) {
            throw new DeploymentException("Application was not started error: " +
                    e.getCause(), applicationId);
        }
    }

    private void waitEntitiesFail(String applicationId, List<String> entityNames) {
        //List<String> namesInLowerCase = entitiyNames.stream().map(String::toLowerCase).collect(Collectors.toList());
        //Iterable<EntitySummary> summaries = deployerProxy.getDescendants(applicationId);

        List<EntitySummary> sumaries = deployerProxy.getEntitiesByNames(applicationId, entityNames);

        boolean allStopped = sumaries.stream()
                .map(EntitySummary::getId)
                .allMatch(id -> waitEntityIsNotUp(applicationId, id));

        if (!allStopped) {
            throw new RuntimeException("Entities no paradas a tiemp");
        }
    }


    private String getBluePrint(String name) throws URISyntaxException, IOException {
        URL blueprint = getClass().getClassLoader().getResource(name + "/" + name + "-brooklyn.yaml");
        return Resources.toString(blueprint, Charsets.UTF_8);
    }

    private String getMmBluePrint(String name) throws URISyntaxException, IOException {
        URL blueprint = getClass().getClassLoader().getResource(name + "/" + name + "-mm.yaml");
        return Resources.toString(blueprint, Charsets.UTF_8);
    }

    private String getNodesFile(String name) throws URISyntaxException, IOException {
        URL blueprint = getClass().getClassLoader().getResource(name + "/" + "nodes.yaml");
        return Resources.toString(blueprint, Charsets.UTF_8);
    }

    private Nodes nodesToOperate(Experiment ex) throws Exception {

        Yaml yaml = new Yaml(new Constructor(Nodes.class));
        Nodes nodes = yaml.load(getNodesFile(ex.toString()));

        return nodes;
    }

    public static class Nodes {
        private Provider aws;
        private Provider pivotal;

        public Nodes() {
            aws = new Provider();
            pivotal = new Provider();
        }

        public Provider getAws() {
            return aws;
        }

        public void setAws(Provider aws) {
            this.aws = aws;
        }

        public Provider getPivotal() {
            return pivotal;
        }

        public void setPivotal(Provider pivotal) {
            this.pivotal = pivotal;
        }

        public List<String> getAllEntitiesToCrach() {
            return Stream.concat(getAws().getCrash().stream(), getPivotal().getCrash().stream()).collect(toList());
        }
    }

    public static class Provider {

        private List<String> crash = new LinkedList<>();
        private List<String> stop = new LinkedList<>();
        private List<String> duringRecovery = new LinkedList<>();

        public List<String> getDuringRecovery() {
            return duringRecovery;
        }

        public void setDuringRecovery(List<String> duringRecovery) {
            this.duringRecovery = duringRecovery;
        }

        public List<String> getCrash() {
            return crash;
        }

        public void setCrash(List<String> crash) {
            this.crash = crash;
        }

        public List<String> getStop() {
            return stop;
        }

        public void setStop(List<String> stop) {
            this.stop = stop;
        }
    }

}