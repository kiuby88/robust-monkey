package org.scenic.parallel.extraction;

import java.util.List;
import java.util.stream.Collectors;

import org.scenic.experiment.Experiment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Jose on 23/10/19.
 */
public class ExperimentInstanceTasks {
    private int offset;

    private Experiment scenario;

    private List<TaskDescription> tasks;

    public ExperimentInstanceTasks(List<TaskDescription> tasks){
        this.tasks=tasks;
    }

    public List<TaskDescription> getTasks() {
        return tasks;
        //return tasks.stream().filter(t -> t!=null).collect(Collectors.toList());
    }

    public int getOffset() {
        return offset;
    }

    public Experiment getScenario() {
        return scenario;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void setScenario(Experiment scenario) {
        this.scenario = scenario;
    }

    public void setTasks(List<TaskDescription> tasks) {
        this.tasks = tasks.stream().filter(t -> t!=null).collect(Collectors.toList());
    }

    @Override
    public String toString(){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
