package org.scenic.parallel.extraction;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.brooklyn.rest.domain.LinkWithMetadata;
import org.apache.brooklyn.rest.domain.TaskSummary;
import org.apache.brooklyn.util.collections.MutableList;
import org.apache.brooklyn.util.time.Duration;
import org.scenic.proxy.DeployerProxy;

import com.google.common.collect.ImmutableList;

/**
 * Created by Jose on 23/10/19.
 */
public class TaskExtractor {

    private final DeployerProxy proxy;

    private final String applicationId;
    private final long startTime;

    private static final List<String> supportedTasks = ImmutableList.of("start", "stop", "restart");

    public TaskExtractor(DeployerProxy proxy, String applicationId, long startTime) {
        this.proxy = proxy;
        this.applicationId = applicationId;
        this.startTime = startTime;
    }


    public ExperimentInstanceTasks extract() {

        //TaskSummary t = proxy.getTask(taskId);
        return new ExperimentInstanceTasks(extractOfApplicationEntities(applicationId));
    }

    //public TaskErrors extractDeploymentErrors() {
    //    TaskSummary t = proxy.getTask(taskId);
    //    return new TaskErrors(KindOfError.DEPLOY, extractFails(t));
    //}

    //public TaskErrors extractMigrationErrors() {
    //    TaskSummary t = proxy.getTask(taskId);
    //    return new TaskErrors(KindOfError.MIGRATION, extractFails(t));
    //}

    private List<TaskDescription> extractOfApplicationEntities(String applicationId) {

        return proxy.getDescendants(applicationId)
                .stream()
                .flatMap(entity -> proxy.tasksOfEntity(applicationId, entity.getId()).stream())
                .filter(t -> t.getStartTimeUtc() > startTime)
                .filter(t -> supportedTasks.contains(t.getDisplayName().toLowerCase()))
                .map(this::mapToDesc)
                .collect(Collectors.toList());





/*
        TaskDescription d = null;
        System.out.println("***" + t.getDisplayName());
        if (
                (!t.getDisplayName().contains("#")
                        && !t.getDisplayName().contains("(parallel)")
                        //&& ! t.getDisplayName().contains("migra")
                )
                        || (t.getDisplayName().contains("release task #"))
                ) {

            d = new TaskDescription();
            d.setEntityName(t.getEntityDisplayName());
            d.setStartTime(t.getStartTimeUtc());
            d.setEndTime(t.getEndTimeUtc());
            d.setTaskName(t.getDisplayName());
            d.setTaskId(t.getId());

            d.setDuration(Duration
                    .millis(t.getEndTimeUtc() - t.getStartTimeUtc()).toMilliseconds());


            if (!t.getDisplayName().contains("migra")) {
                return MutableList.of(d);
            }
        }

        List<TaskDescription> taskDescriptions = MutableList.of();
        taskDescriptions.add(d);
        */

        //for (LinkWithMetadata link : t.getChildren()) {
        //    taskDescriptions.addAll(extract(proxy.getTask((String) link.getMetadata().get("id"))));
       // }

        //return taskDescriptions;
    }

    private TaskDescription mapToDesc(TaskSummary t){

        System.out.println("***" + t.getDisplayName());

        TaskDescription d = new TaskDescription();
            d.setEntityName(t.getEntityDisplayName());
            d.setStartTime(t.getStartTimeUtc());
            d.setEndTime(t.getEndTimeUtc());
            d.setTaskName(t.getDisplayName());
            d.setTaskId(t.getId());

        System.out.println(t.getEndTimeUtc());
        System.out.println(t.getStartTimeUtc());
            d.setDuration(Duration
                    .millis(t.getEndTimeUtc() - t.getStartTimeUtc()).toMilliseconds());
        return d;
    }


    /*
    private List<TaskErrorDescription> extractFails(TaskSummary t) {


        TaskErrorDescription d = null;
        if (
                (!t.getDisplayName().contains("#")
                        && !t.getDisplayName().contains("(parallel)")
                        && (t.isError() || t.isCancelled())
                        //&& ! t.getDisplayName().contains("migra")
                )
            //Necesito traer todos los datos de los errores, incluso release
            //|| (t.getDisplayName().contains("release task #"))
                ) {

            d = new TaskErrorDescription();

            d.setEntityName(t.getEntityDisplayName());
            d.setStartTime(t.getStartTimeUtc());
            d.setEndTime(t.getEndTimeUtc());
            d.setTaskName(t.getDisplayName());
            d.setTaskId(t.getId());
            d.setError(t.isError());
            d.setCancell(t.isError());
            d.setCurrentStatus(t.getCurrentStatus());
            d.setDetailedStatus(t.getDetailedStatus());


            d.setDuration(Duration
                    .millis(t.getEndTimeUtc() - t.getStartTimeUtc()).toMilliseconds());

            //return MutableList.of(d);
        }

        List<TaskErrorDescription> taskDescriptions = MutableList.of();

        for (LinkWithMetadata link : t.getChildren()) {
            String ss = (String) link.getMetadata().get("id");
            try {
                TaskSummary tt = proxy.getTask((String) link.getMetadata().get("id"));
                taskDescriptions.addAll(extractFails(tt));
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        if (d != null) {
            taskDescriptions.add(d);
        }

        return taskDescriptions;
    }*/

}
