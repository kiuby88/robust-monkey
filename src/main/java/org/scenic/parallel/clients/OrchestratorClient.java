package org.scenic.parallel.clients;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

/**
 * Created by Jose on 17/10/19.
 */
public class OrchestratorClient {

    private static final String DEFAULT_URL = "http://localhost:8085";

    String url;
    OkHttpClient client;

    public static OrchestratorClient build() {
        return new OrchestratorClient(DEFAULT_URL);
    }

    private OrchestratorClient(String url) {
        this.url = url;
        client = new OkHttpClient();
    }

    public void evict() {
        client.getConnectionPool().evictAll();
    }

    public void update() {
        try {
            Request request = new Request.Builder()
                    .url(this.url + "/update")
                    .put(null)
                    .build();
            client.newCall(request).execute();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void sycn(String appId, String blueprint) {
        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, blueprint);

            Request request = new Request.Builder()
                    .url(this.url + "/sync/" + appId)
                    .put(body)
                    .build();
            client.newCall(request).execute();
        } catch (Exception e) {
            throw new RuntimeException("Error in the sync of application ", e);
        }
    }

    public void sycnFromPath(String appId, String path) throws Exception {
        sycn(appId, getMmBluePrint(path));
    }

    private static String getMmBluePrint(String path) throws URISyntaxException, IOException {
        URL blueprint = OrchestratorClient.class.getClassLoader().getResource(path);
        return Resources.toString(blueprint, Charsets.UTF_8);
    }
}
