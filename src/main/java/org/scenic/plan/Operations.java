package org.scenic.plan;

/**
 * Created by Jose on 16/02/19.
 */
public enum Operations {

    RELEASE("stop"), START("start"), STOP("stop"), RESTART("restart") ;

    private final String value;

    Operations(String value) {
        this.value=value;
    }

    public String getValue(){
        return value;
    }


}
