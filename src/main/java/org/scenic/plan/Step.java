package org.scenic.plan;

/**
 * Created by Jose on 15/02/19.
 */
public class Step {

    private String node;
    private Operations operation;

    public Step(){

    }

    public Operations getOperation() {
        return operation;
    }

    public void setOperation(Operations operation) {
        this.operation = operation;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }
}
