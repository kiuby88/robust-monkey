package org.scenic.plan;

import java.util.Map;

import org.apache.brooklyn.rest.domain.TaskSummary;
import org.apache.brooklyn.util.collections.MutableMap;
import org.scenic.experiment.ExperimentExecutionTrace;
import org.scenic.exception.PlanException;
import org.scenic.plan.Operations;
import org.scenic.plan.Step;
import org.scenic.proxy.DeployerProxy;
import org.scenic.proxy.EffectorConfig;
import org.scenic.tasks.TaskDescription;
import org.scenic.tasks.TaskExtractor;

/**
 * Created by Jose on 16/02/19.
 */
public class PlanStepExecutor {

    private static final String STOP_MACHINE_MODE = "stopMachineMode";
    private static final String STOP_MACHINE_IF_NOT_STOPPED = "IF_NOT_STOPPED";
    private static final String NEVER = "NEVER";

    private final TaskExtractor taskExtractor;
    private DeployerProxy deployerProxy;

    public PlanStepExecutor() {
        deployerProxy = DeployerProxy.newInstance();
        taskExtractor = new TaskExtractor(deployerProxy);
    }

    public void execute(ExperimentExecutionTrace experimentExecutionTrace, Step step) {
        EffectorConfig config = getEffectorConfig(experimentExecutionTrace, step);
        System.out.println("**Execute task => " + step.getNode() + ":"+step.getOperation() );
        TaskSummary taskSummary = deployerProxy.executeEffector(config);
        TaskDescription taskDescription = taskExtractor.extract(taskSummary);
        experimentExecutionTrace.addTaskDescriptions(taskDescription);
    }

    private EffectorConfig getEffectorConfig(ExperimentExecutionTrace experimentExecutionTrace, Step step) {
        String appId = experimentExecutionTrace.getApplicationId();
        String entityId = deployerProxy.getEntityIdByDisplayName(appId, step.getNode());

        EffectorConfig config = new EffectorConfig();
        config.setApplicationId(appId);
        config.setEntityId(entityId);
        config.setEffectorName(step.getOperation().getValue());
        config.setParameters(parameters(step.getOperation()));

        return config;
    }

    private Map<String, Object> parameters(Operations operation){

        if(operation == Operations.START) {
            return getStartParameters();
        }

        if(operation == Operations.RELEASE) {
            return getReleaseParameters();
        }

        if(operation == Operations.STOP) {
            return getStopParameters();
        }

        if(operation == Operations.RESTART) {
            return getRestartParameters();
        }

        throw new PlanException("Missing effector parameters for operation " + operation);

    }


    public Map<String, Object> getReleaseParameters() {

        return MutableMap.of(STOP_MACHINE_MODE, STOP_MACHINE_IF_NOT_STOPPED);
    }

    public Map<String, Object> getStopParameters() {
        return MutableMap.of(STOP_MACHINE_MODE, NEVER);
    }

    public Map<String, Object> getStartParameters() {
        return MutableMap.of("location", "NONE");
    }

    public Map<String, Object> getRestartParameters() {
        return MutableMap.of();
    }
}
