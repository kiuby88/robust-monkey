package org.scenic.proxy;

import static java.util.stream.Collectors.toList;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.time.Instant;

import org.cloudfoundry.client.lib.CloudCredentials;
import org.cloudfoundry.client.lib.CloudFoundryClient;
import org.cloudfoundry.client.lib.domain.CloudEntity;

/**
 * Created by Jose on 20/01/20.
 */
public class TestingPivotalClient {

    private static CloudFoundryClient CLIENT;

    public static void main(String[] args) throws InterruptedException {

        int seg = 20;
        init();
        int i=0;
        int limit =5000;
        while(i<limit){

            System.out.println("--------------------------");
            System.out.println(Instant.now().toString() +": --- Trying to execute ---");
            System.out.println("Checking token expirationDate" + CLIENT.login().getExpiration());
            System.out.println("Checking token value" + CLIENT.login().getValue());
            System.out.println("Checking token expiration in " + CLIENT.login().getExpiresIn());
            if(CLIENT.login().isExpired()){
                System.out.println("Client is expired. Refresing ");
                CLIENT.login().getRefreshToken();
            }
            System.out.println();
            Thread.sleep(1000l * seg);
            i++;
        }


    }

    private static void init(){
        CloudCredentials credentials =
                new CloudCredentials("user", "pass");
        CLIENT = new CloudFoundryClient(credentials, getTargetURL("https://api.run.pivotal.io"),
                "gsoc", "development", true);
        CLIENT.login();

    }
    private static URL getTargetURL(String target) {
        try {
            return URI.create(target).toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException("The target URL is not valid: " + e.getMessage());
        }
    }
}
