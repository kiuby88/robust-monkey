package org.scenic.proxy;

import java.util.Map;

/**
 * Created by Jose on 16/02/19.
 */
public class EffectorConfig {


    private static final String TIMEOUT = "never";

    private String applicationId;

    private String entityId;

    private String effectorName;

    private String timeout = TIMEOUT;

    private Map<String, Object> parameters;


    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEffectorName() {
        return effectorName;
    }

    public void setEffectorName(String effectorName) {
        this.effectorName = effectorName;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }
}
