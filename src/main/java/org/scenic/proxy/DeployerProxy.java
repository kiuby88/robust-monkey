package org.scenic.proxy;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.ws.rs.core.Response;

import org.apache.brooklyn.launcher.BrooklynLauncher;
import org.apache.brooklyn.rest.api.ApplicationApi;
import org.apache.brooklyn.rest.api.EffectorApi;
import org.apache.brooklyn.rest.client.BrooklynApi;
import org.apache.brooklyn.rest.domain.ApplicationSummary;
import org.apache.brooklyn.rest.domain.EntitySummary;
import org.apache.brooklyn.rest.domain.LocationSummary;
import org.apache.brooklyn.rest.domain.TaskSummary;
import org.apache.brooklyn.util.collections.MutableMap;
import org.scenic.exception.DeploymentException;

import com.google.common.base.Optional;

public class DeployerProxy {

    public final static String DEFAULT_SERVER_URL = "http://127.0.0.1:8081";
    private final static String DEFAULT_REGEX = ".*";
    private static final String STOP = "stop";
    private static final String NEVER = "never";
    private static final String MIGRATE_EFFECTOR_NAME = "migrate";

    private BrooklynLauncher launcher;
    private static DeployerProxy proxy;
    private final ApplicationApi applicationApi;
    public final BrooklynApi api;

    public static DeployerProxy newInstance() {
        if (proxy == null) {
            proxy = new DeployerProxy();
        }
        return proxy;
    }

    public static DeployerProxy newInstance(String deployerUrl) {
        if (proxy == null) {
            proxy = new DeployerProxy(deployerUrl);
        }
        return proxy;
    }

    public static DeployerProxy connectDefault() {
        if (proxy == null) {
            proxy = new DeployerProxy(DEFAULT_SERVER_URL);
        }
        return proxy;
    }

    private DeployerProxy() {
        BrooklynLauncher.newInstance().start();
        api = new BrooklynApi(DEFAULT_SERVER_URL);
        applicationApi = api.getApplicationApi();
    }

    private DeployerProxy(String deployerUrl) {
        api = new BrooklynApi(deployerUrl);
        applicationApi = api.getApplicationApi();
    }

    public TaskSummary executeEffector(EffectorConfig effectorConfig) {
        EffectorApi effectorApi = api.getEffectorApi();

        effectorApi.invoke(effectorConfig.getApplicationId(),
                effectorConfig.getEntityId(),
                effectorConfig.getEffectorName(),
                effectorConfig.getTimeout(),
                effectorConfig.getParameters());

        List<TaskSummary> tasks = api.getEntityApi().listTasks(effectorConfig.getApplicationId(), effectorConfig.getEntityId());

        List<TaskSummary> effectorTasks =
                tasks.stream()
                        .filter(t -> t.getDisplayName().equals(effectorConfig.getEffectorName()))
                        .collect(Collectors.toList());

        return effectorTasks.get(effectorTasks.size() - 1);
    }

    public TaskSummary getTask(String taskId) {
        return api.getActivityApi().get(taskId);
    }

    public void deleteApplication(String entityId) {
        api.getEffectorApi()
                .invoke(entityId, entityId, STOP, NEVER, MutableMap.<String, Object>of());
    }

    public TaskSummary deployApplicationFromYaml(String blueprint) {
        Response a = applicationApi.createFromYaml(blueprint);
        return (TaskSummary) a.getEntity();
    }

    public TaskSummary migrateApplication(String appId, String params) throws Exception {
        api.getEffectorApi()
                .invoke(appId, appId, "migrateChildren", "never", MutableMap.<String, Object>of("childrenLocationsSpec", params));

        TaskSummary taskFound = null;

        List<TaskSummary> tasks = api.getEntityApi().listTasks(appId, appId);
        for (TaskSummary task : tasks) {
            if (task.getDisplayName().contains("migrate")) {
                //if (!task.isError()) {
                return task;
                //}
                //taskFound = task;
                //throw new Exception("Error migrating. Task is error");
            }
        }
        //if(taskFound == null){
        throw new Exception("Error migrating. Task not found");
        //}
        //throw new Exception("Error migrating. Task was found with Error\n" + taskFound.getDetailedStatus());

    }

    public TaskSummary migrateComponent(String appId, String entityId, String newLocation) {
        Response a = api.getEffectorApi()
                .invoke(appId, entityId, MIGRATE_EFFECTOR_NAME, "never", MutableMap.<String, Object>of("locationSpec", newLocation));
        return (TaskSummary) a.getEntity();
    }

    public List<EntitySummary> getDescendants(final String applicationId) {
        return applicationApi.getDescendants(applicationId, DEFAULT_REGEX);
        //return Iterables.filter(descendants, new Predicate<EntitySummary>() {
        //    @Override
        //    public boolean apply(EntitySummary input) {
        //        return !input.getId().equals(applicationId);
        //    }
        //});
    }

    public List<TaskSummary> tasksOfEntity(String applicatioinId, String entityId) {
        return api.getEntityApi().listTasks(applicatioinId, entityId).stream().collect(Collectors.toList());
    }

    public List<EntitySummary> getEntitiesByNames(String applicationId, List<String> entityNames) {

        List<String> namesInLowerCase = entityNames.stream().map(String::toLowerCase).collect(Collectors.toList());
        Iterable<EntitySummary> summaries = getDescendants(applicationId);
        return StreamSupport.stream(summaries.spliterator(), false)
                .filter(s -> namesInLowerCase.contains(s.getName().toLowerCase()))
                .collect(Collectors.toList());
    }

    public Optional<ApplicationSummary> getApplication(String applicationId) {
        try {
            return Optional.of(applicationApi.get(applicationId));
        } catch (Exception e) {
            return Optional.absent();
        }
    }


    public String getEntityOfTask(String taskId) {
        return getTask(taskId).getEntityId();
    }


    public String getEntityDisplayName(String applicationId, String entityId) {
        return api.getEntityApi().get(applicationId, entityId).getName();
    }

    public String getEntityIdByDisplayName(String applicationId, String entityDisplayName) {
        String entityId;
        for (EntitySummary eS : getDescendants(applicationId)) {
            if (eS.getName().equals(entityDisplayName)) {
                return eS.getId();
            }
        }
        return null;
    }

    public List<LocationSummary> getLocationOfEntityOrParent(String applicationId, String entityId) {
        List<LocationSummary> entityLocations =
                proxy.api.getEntityApi().getLocations(applicationId, entityId);
        if ((entityLocations == null) || (entityLocations.isEmpty())) {
            entityLocations = getLocationOfApplication(applicationId);
        }
        return entityLocations;
    }

    public List<LocationSummary> getLocationOfApplication(String applicationId) {
        return proxy.api.getEntityApi().getLocations(applicationId, applicationId);
    }

    public LocationSummary getLocation(String locationId) {
        return proxy.api.getLocationApi().get(locationId, null);
    }

    public FutureTask<Boolean> applicationIsUp(String applicationId) {
        return isUp(applicationId, applicationId);
    }

    public FutureTask<Boolean> applicationWillBeUp(String applicationId) {
        return willAllEntitiesUp(applicationId);
    }

    public FutureTask<Boolean> entityIsUp(String applicationId, String entityId) {
        return isUp(applicationId, entityId);
    }

    private FutureTask<Boolean> isUp(final String applicationId, final String entityId) {
        return new FutureTask<Boolean>(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                boolean isUp = false;
                int cycles = 0;
                while ((cycles < 600 && !isUp) && !isOnFire(applicationId, entityId)) {
                    isUp = isUpSensor(applicationId, entityId);
                    //System.out.println("Sensor==>" +  isUp);
                    Thread.sleep(3000);
                    cycles++;
                }
                if (!isUp) {
                    throw new DeploymentException("Entity " + entityId + " does not look up, " +
                            "in application " + applicationId + " context", applicationId);
                }
                return true;
            }
        }
        );
    }


    private FutureTask<Boolean> willBeUp(final String applicationId, final String entityId) {
        return new FutureTask<Boolean>(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                boolean isUp = false;
                int cycles = 0;
                while ((cycles < 600 && !isUp)) {
                    isUp = isUpSensor(applicationId, entityId);
                    //System.out.println("Sensor==>" +  isUp);
                    Thread.sleep(15_000);
                    cycles++;
                }
                if (!isUp) {
                    throw new DeploymentException("Entity " + entityId + " does not look up, " +
                            "in application " + applicationId + " context", applicationId);
                } else {
                    System.out.println("-- Application is UP ");
                }
                return true;
            }
        }
        );
    }

    private FutureTask<Boolean> willAllEntitiesUp(final String applicationId) {

        List<String> ids = getDescendants(applicationId).stream().map(EntitySummary::getId).collect(Collectors.toList());

        return new FutureTask<>(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                boolean areUp = false;
                int cycles = 0;
                while ((cycles < 600 && !areUp)) {
                    Thread.sleep(15_000);
                    areUp = ids.stream().allMatch(eId -> isUpSensor(applicationId, eId));
                    System.out.println("Are all up?" + areUp);
                    //isUpSensor(applicationId, entityId);
                    //System.out.println("Sensor==>" +  isUp);
                    cycles++;
                }
                if (!areUp) {
                    throw new DeploymentException("All entities  does not look up, " +
                            "in application " + applicationId + " context", applicationId);
                } else {
                    System.out.println("-- Application is UP " + ids.stream().allMatch(eId -> isUpSensor(applicationId, eId)) );
                }
                return true;
            }
        });
    }


    public FutureTask<Boolean> isNotUp(final String applicationId, final String entityId) {
        return new FutureTask<Boolean>(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                boolean isUp = true;
                int cycles = 0;
                while ((cycles < 600 && isUp) && !isOnFire(applicationId, entityId)) {
                    isUp = isUpSensor(applicationId, entityId);
                    Thread.sleep(3000);
                    cycles++;
                }
                if (isUp && !isOnFire(applicationId, entityId)) {
                    throw new DeploymentException("Entity " + entityId + " look up, " +
                            "in application " + applicationId + " context", applicationId);
                }
                return true;
            }
        }
        );
    }

    public String getPlainSensor(String application, String entity, String sensorName) {
        return api.getSensorApi().getPlain(application, entity, sensorName, true);
    }


    private boolean isUpSensor(String applicationId, String entityId) {
        return (Boolean) api.getSensorApi().get(applicationId, entityId, "service.isUp", false);
    }

    private boolean isOnFire(String applicationId, String entityId) {
        return "ON_FIRE".equalsIgnoreCase((String) api.getSensorApi().get(applicationId, entityId, "service.state", true));
    }


    public String getStatus(String applicationId, String entityId) {
        return (String) api.getSensorApi().get(applicationId, entityId, "service.state", true);
    }
}
