package org.scenic.proxy;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;

import org.cloudfoundry.client.lib.CloudCredentials;
import org.cloudfoundry.client.lib.CloudFoundryClient;
import org.cloudfoundry.client.lib.domain.CloudApplication;
import org.cloudfoundry.client.lib.domain.CloudApplication.AppState;

/**
 * Created by Jose on 09/12/19.
 */
public class CloudFoundryProviderClient {


    private static CloudFoundryClient CLIENT;

    public CloudFoundryProviderClient() {
        setUpClient();
    }

    private static void setUpClient() {
        if (CLIENT == null) {
            CloudCredentials credentials =
                    new CloudCredentials("user", "pass");
            CLIENT = new CloudFoundryClient(credentials, getTargetURL("https://api.run.pivotal.io"),
                    "gsoc", "development", true);
            CLIENT.login();
        }
    }

    public void login() {
        for (int i = 0; i < 10; i++)
            try {
                CLIENT.login();
                return;
            } catch (Exception e) {
                System.out.println("Error in Pivotal login " + e.getCause());
                if (i == 9) {
                    throw e;
                }
            }
    }

    public void logout() {
        for (int i = 0; i < 10; i++)
            try {
                CLIENT.logout();
                return;
            } catch (Exception e) {
                System.out.println("Error in Pivotal logout" + e.getCause());
                if (i == 9) {
                    throw e;
                }
            }
    }

    private static URL getTargetURL(String target) {
        try {
            return URI.create(target).toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException("The target URL is not valid: " + e.getMessage());
        }
    }

    public void stopApplication(String applicationName) {
        CLIENT.stopApplication(applicationName);
    }

    public List<CloudApplication> getApplications() {
        return CLIENT.getApplications();
    }

    public void deleteApplication(String applicationName) {

        CLIENT.deleteApplication(applicationName);
    }

    public AppState getStatus(String name) {
        return CLIENT.getApplication(name).getState();
    }

}
