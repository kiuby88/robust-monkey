package org.scenic.cleaner;

/**
 * Created by Jose on 08/04/18.
 */
public class AwsCleaner {


    private static Ec2CleanUp cleanerI;
    private static Ec2CleanUp cleanerO;

    public static void main(String[] args) {
        clean();
    }

    public static void clean() {
        try {
            if (cleanerI == null) {
                cleanerI =
                        new Ec2CleanUp("eu-west-1", "*", "identity", "credential", false);
                cleanerO =
                        new Ec2CleanUp("us-west-2", "*", "identity", "credential", false);
            }
            cleanerI.cleanUp();
            cleanerO.cleanUp();
        } catch (Exception e) {
            System.out.println("Error cleanning");
        }
    }


}
