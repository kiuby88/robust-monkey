package org.scenic.cleaner.entity;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.brooklyn.rest.domain.EntitySummary;
import org.jclouds.ContextBuilder;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.domain.NodeMetadata;
import org.jclouds.compute.domain.internal.NodeMetadataImpl;
import org.jclouds.logging.slf4j.config.SLF4JLoggingModule;
import org.jclouds.sshj.config.SshjSshClientModule;
import org.scenic.proxy.DeployerProxy;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Module;

/**
 * Created by Jose on 09/12/19.
 */
public class AwsEntityCleaner {

    private DeployerProxy deployerProxy;

    public AwsEntityCleaner(DeployerProxy deployerProxy) {
        this.deployerProxy = deployerProxy;
    }

    public void cleanEntities(String applicationId, List<String> entities) {
        this.cleanEntitySummaries(applicationId, mapToEntitySummaries(applicationId, entities));
    }

    private void cleanEntitySummaries(String applicationId, List<EntitySummary> entities) {

        if (entities.isEmpty()) {
            return;
        }

        ImmutableSet<Module> modules = ImmutableSet.<Module>of(new SLF4JLoggingModule());

        ComputeServiceContext computeServiceContext = ContextBuilder.newBuilder("aws-ec2")
                .credentials("identity", "credential")
                .modules(ImmutableSet.<Module>of(new SLF4JLoggingModule(),
                        new SshjSshClientModule()))
                .buildView(ComputeServiceContext.class);

        List<? extends NodeMetadataImpl> listNodes =
                computeServiceContext
                        .getComputeService().listNodes()
                        .stream()
                        .filter(NodeMetadataImpl.class::isInstance)
                        .map(NodeMetadataImpl.class::cast)
                        .filter(n -> n.getStatus().equals(NodeMetadata.Status.RUNNING))
                        .collect(Collectors.toList());


        List<String> entitiesId = entities.stream().map(EntitySummary::getId).collect(Collectors.toList());

        System.out.println("Init-- le la ejecicion del borrado desde el codigo ");
        System.out.println(Instant.now().toString());
        System.out.println("********************************************************");


        //computeServiceContext.getComputeService().destroyNodesMatching(nodeMetadata -> address.contains(nodeMetadata.get))
        computeServiceContext.getComputeService().destroyNodesMatching(nodeMetadata -> {
            boolean r = applicationId.equals(nodeMetadata.getUserMetadata().get("brooklyn-app-id"))
                    && entitiesId.contains(nodeMetadata.getUserMetadata().get("brooklyn-entity-id"));
            System.out.println("---- Listo para borrar? " + r + ": " + nodeMetadata.getUserMetadata().get("brooklyn-entity-id"));
            return r;
        });
        System.out.println("FinishTask le la ejecicion del borrado desde el codigo ");
        System.out.println(Instant.now().toString());
        System.out.println("********************************************************");
        computeServiceContext.close();

    }


    public void cleanMachinesWhenRunning(String applicationId, List<String> entityNames) throws InterruptedException {

        if (entityNames == null || entityNames.isEmpty()) {
            return;
        }

        System.out.println(" -- Deleting: -- ");
        List<EntitySummary> entities = mapToEntitySummaries(applicationId, entityNames);
        List<String> entitiesId = entities.stream().map(EntitySummary::getId).collect(Collectors.toList());


        ComputeServiceContext computeServiceContext = ContextBuilder.newBuilder("aws-ec2")
                .credentials("identity", "credential")
                .modules(ImmutableSet.<Module>of(new SLF4JLoggingModule(),
                        new SshjSshClientModule()))
                .buildView(ComputeServiceContext.class);


        ComputeService computerService = computeServiceContext
                .getComputeService();


        List<? extends NodeMetadataImpl> listNodes = new LinkedList<>();

        System.out.println("Started to check to delete during recovery");
        int cicles = 0;
        while (listNodes.isEmpty() && cicles < 600) {
            listNodes = computerService.listNodes()
                    .stream()
                    .filter(NodeMetadataImpl.class::isInstance)
                    .map(NodeMetadataImpl.class::cast)
                    .filter(n -> n.getStatus().equals(NodeMetadata.Status.RUNNING))
                    .filter(nodeMetadata -> {
                        boolean r = applicationId.equals(nodeMetadata.getUserMetadata().get("brooklyn-app-id"))
                                && entitiesId.contains(nodeMetadata.getUserMetadata().get("brooklyn-entity-id"));
                        return r;
                    })
                    .collect(Collectors.toList());

            if (listNodes.size() != entities.size()) {
                listNodes = new LinkedList<>();
                Thread.sleep(15_000);
            }
            cicles++;
        }

        System.out.println("Detected RUNNING Virtual Machines");
        entities.forEach(e -> System.out.println("-" + e.getName()));


        System.out.println("Init During Recovery-- le la ejecicion del borrado desde el codigo ");
        System.out.println(Instant.now().toString());
        System.out.println("********************************************************");


        //computeServiceContext.getComputeService().destroyNodesMatching(nodeMetadata -> address.contains(nodeMetadata.get))
        computerService.destroyNodesMatching(nodeMetadata -> {
            boolean r = applicationId.equals(nodeMetadata.getUserMetadata().get("brooklyn-app-id"))
                    && entitiesId.contains(nodeMetadata.getUserMetadata().get("brooklyn-entity-id"));
            System.out.println("---- Listo para borrar during Revocery? " + r + ": " + nodeMetadata.getUserMetadata().get("brooklyn-entity-id"));
            return r;
        });
        System.out.println("FinishTask during Recovery la ejecicion del borrado desde el codigo ");
        System.out.println(Instant.now().toString());
        System.out.println("********************************************************");
        computeServiceContext.close();
    }

    private List<EntitySummary> mapToEntitySummaries(String applicationId, List<String> entitiyNames) {
        List<String> namesInLowerCase = entitiyNames.stream().map(String::toLowerCase).collect(Collectors.toList());
        Iterable<EntitySummary> summaries = deployerProxy.getDescendants(applicationId);
        return StreamSupport.stream(summaries.spliterator(), false)
                .filter(s -> namesInLowerCase.contains(s.getName().toLowerCase()))
                .collect(Collectors.toList());
    }


}
