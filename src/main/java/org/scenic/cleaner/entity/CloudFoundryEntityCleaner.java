package org.scenic.cleaner.entity;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.StreamSupport;

import org.apache.brooklyn.rest.domain.EntitySummary;
import org.cloudfoundry.client.lib.domain.CloudApplication;
import org.scenic.proxy.CloudFoundryProviderClient;
import org.scenic.proxy.DeployerProxy;

public class CloudFoundryEntityCleaner {

    private final DeployerProxy deployerProxy;
    private final CloudFoundryProviderClient client;

    public CloudFoundryEntityCleaner(DeployerProxy proxy) {
        this.client = new CloudFoundryProviderClient();
        this.deployerProxy = proxy;
    }

    public void cleanEntities(String applicationId, List<String> names) {
        if (!names.isEmpty()) {
            System.out.println("Deleting cf entities " + names.toString());

            for (String name : mapToSensorName(applicationId, names)) {
                System.out.println("Deleting cf entity " + name);
                client.deleteApplication(name);
            }

        }
    }

    private List<String> mapToSensorName(String applicationId, List<String> entitiyNames) {
        List<String> namesInLowerCase = entitiyNames.stream().map(String::toLowerCase).collect(toList());
        Iterable<EntitySummary> summaries = deployerProxy.getDescendants(applicationId);
        return StreamSupport.stream(summaries.spliterator(), false)
                .filter(s -> namesInLowerCase.contains(s.getName().toLowerCase()))
                .map(e -> toStringList(deployerProxy.getPlainSensor(applicationId, e.getId(), "webapp.deployedWars")).get(0))
                .collect(toList());
    }

    private List<String> toStringList(String value) {
        return stream(value.replace("[", "").replace("]", "").split(",")).map(String::trim).collect(toList());
    }

    public void cleanApplicationsWhenRunning(String applicationId, List<String> entityNames) throws InterruptedException {

        client.login();
        if ((entityNames == null) || (entityNames.isEmpty())) {
            return;
        }

        System.out.println(" -- Deleting Applications: -- ");
        List<String> entities = mapToSensorName(applicationId, entityNames);

        boolean allRunning = false;
        int cycles = 0;

        System.out.println("CloudFoundry: check if all are stoper");
        while ((!allRunning) && (cycles < 600)) {
            allRunning = entities.stream().allMatch(entityName -> client.getStatus(entityName) == CloudApplication.AppState.UPDATING);
            Thread.sleep(15_000l);
            cycles++;
        }
        System.out.println("CloudFoundry: ALL ARE STOPPED");

        entities.stream()
                .forEach(e -> {
                    try {
                        System.out.println("Deleting Application: " + e);
                        client.deleteApplication(e);
                        System.out.println("Deleted Application: " + e);
                    } catch (Exception ex) {
                        System.out.println("Error deleting application " + e + " because " + ex.getMessage());
                    }
                });
        client.logout();
    }


}
