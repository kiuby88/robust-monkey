package org.scenic.cleaner.entity;


import static java.util.stream.Collectors.toList;
import static org.scenic.parallel.main.ParallelEvaluation.Nodes;

import java.util.List;
import java.util.stream.StreamSupport;

import org.apache.brooklyn.rest.domain.EntitySummary;
import org.scenic.proxy.DeployerProxy;

/**
 * Created by Jose on 13/10/19.
 */
public class EntitiesCleaner {

    private final DeployerProxy deployerProxy;
    private final AwsEntityCleaner awsEntityCleaner;
    private final CloudFoundryEntityCleaner cloudFoundryEntityCleaner;

    public EntitiesCleaner(DeployerProxy deployerProxy) {
        this.deployerProxy = deployerProxy;
        this.awsEntityCleaner = new AwsEntityCleaner(deployerProxy);
        this.cloudFoundryEntityCleaner = new CloudFoundryEntityCleaner(deployerProxy);
    }

    public void cleanEntities(String applicationId, Nodes nodes) {
        cloudFoundryEntityCleaner.cleanEntities(applicationId, nodes.getPivotal().getCrash());
        awsEntityCleaner.cleanEntities(applicationId, nodes.getAws().getCrash());
    }

    public void cleanDuringRecovery(String applicationId, Nodes nodes) {
        try {
            cloudFoundryEntityCleaner.cleanApplicationsWhenRunning(applicationId, nodes.getPivotal().getDuringRecovery());
            awsEntityCleaner.cleanMachinesWhenRunning(applicationId, nodes.getAws().getDuringRecovery());
        } catch (Exception e) {
            System.out.println("Error deleting entities during recovery");
            throw new RuntimeException(e);
        }
    }

    private List<EntitySummary> mapToEntitySummaries(String applicationId, List<String> entitiyNames) {
        List<String> namesInLowerCase = entitiyNames.stream().map(String::toLowerCase).collect(toList());
        Iterable<EntitySummary> summaries = deployerProxy.getDescendants(applicationId);
        return StreamSupport.stream(summaries.spliterator(), false)
                .filter(s -> namesInLowerCase.contains(s.getName().toLowerCase()))
                .collect(toList());
    }

}
