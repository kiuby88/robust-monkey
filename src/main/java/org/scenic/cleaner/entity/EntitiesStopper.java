package org.scenic.cleaner.entity;

import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.scenic.parallel.main.ParallelEvaluation.Nodes;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.brooklyn.rest.domain.EntitySummary;
import org.apache.brooklyn.util.collections.MutableMap;
import org.scenic.proxy.DeployerProxy;
import org.scenic.proxy.EffectorConfig;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;

/**
 * Created by Jose on 28/10/19.
 */
public class EntitiesStopper {

    private static final String STOP_MACHINE_MODE = "stopMachineMode";
    private static final String STOP_MACHINE_IF_NOT_STOPPED = "IF_NOT_STOPPED";

    private static final String STOP_PROCESS_MODE = "stopProcessMode";
    private static final String STOP_PROCESS_MODE_NEVER = "NEVER";

    private final DeployerProxy proxy;

    public EntitiesStopper(DeployerProxy proxy) {
        this.proxy = proxy;
    }


    public void stopEntities(String applicationId, Nodes nodes) {

        List<String> entityNames = Stream.concat(nodes.getAws().getStop().stream(),
                nodes.getPivotal().getStop().stream()).collect(toList());
        stopEntitiesByName(applicationId, entityNames);
    }

    public void stopEntity(String applicationId, String name) {
        stopEntitiesByName(applicationId, singletonList(name));
    }

    private void stopEntitiesByName(String applicationId, List<String> entityNames) {
        List<EntitySummary> entities = proxy.getEntitiesByNames(applicationId, Optional.fromNullable(entityNames).or(ImmutableList.of()));

        entities.stream()
                .map(e -> stopEffector(applicationId, e.getId()))
                .peek(this::printEffectorConfig)
                .forEach(proxy::executeEffector);
    }

    private void printEffectorConfig(EffectorConfig e) {
        System.out.println("----Effector");
        System.out.println(format("Effector name=%s, appId=%s, entityId=%s ", e.getEffectorName(), e.getApplicationId(), e.getEntityId()));
        System.out.println(format("Effector config=%s", e.getParameters().toString()));
    }

    private EffectorConfig stopEffector(String applicationId, String entityId) {

        EffectorConfig effectorConfig = new EffectorConfig();
        effectorConfig.setApplicationId(applicationId);
        effectorConfig.setEntityId(entityId);
        effectorConfig.setEffectorName("stop");
        effectorConfig.setParameters(stopConfig());
        return effectorConfig;
    }

    private Map<String, Object> stopConfig() {
        return MutableMap.of(STOP_MACHINE_MODE, STOP_PROCESS_MODE_NEVER,
                STOP_PROCESS_MODE, STOP_MACHINE_IF_NOT_STOPPED);
    }
}
