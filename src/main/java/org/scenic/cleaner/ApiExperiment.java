package org.scenic.cleaner;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.jclouds.ContextBuilder;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.domain.ComputeMetadata;
import org.jclouds.ec2.EC2Api;
import org.jclouds.ec2.features.InstanceApi;
import org.jclouds.logging.slf4j.config.SLF4JLoggingModule;
import org.jclouds.sshj.config.SshjSshClientModule;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Module;

/**
 * Created by Jose on 05/10/19.
 */
public class ApiExperiment {



    public static void main(String[] args){

        ImmutableSet<Module> modules = ImmutableSet.<Module>of(new SLF4JLoggingModule());
        EC2Api api = ContextBuilder
                .newBuilder("aws-ec2")
                .credentials("identity", "credential")
                .modules(modules)
                .buildApi(EC2Api.class);
        InstanceApi instanceApi = api.getInstanceApi().get();

        ComputeServiceContext computeServiceContext= ContextBuilder.newBuilder("aws-ec2")
                .credentials("identity", "credential")
                .modules(ImmutableSet.<Module> of(new SLF4JLoggingModule(),
                        new SshjSshClientModule()))
                .buildView(ComputeServiceContext.class);


        List<? extends ComputeMetadata> listNodes =
                computeServiceContext
                        .getComputeService().listNodes()
                        .stream()
                        .collect(Collectors.toList());
        List<ComputeMetadata> euWest2Nodes = listNodes.stream().filter(p -> p.getLocation().getDescription().equals("us-west-2c")).collect(Collectors.toList());


        System.out.println(euWest2Nodes);



    }


}
